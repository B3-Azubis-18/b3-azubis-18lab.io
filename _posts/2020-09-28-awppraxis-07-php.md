---
layout: post
title: "07. PHP"
date: 2020-03-20 10:10:00 +0200
category: 2. Lehrjahr
tags: [Anwendungsprogrammierung, Praxis]
---

## Was ist PHP?

PHP ist eine Skriptsprache, die in HTML eingebettet wird. Bevor die Seite vom Server ausgeliefert wird, muss sie von
einem PHP-Interpreter zu reinem HTML übersetzt werden.

```php
<html>
    <head>
        <title>PHP-Test</title>
    </head>
    <body>
        <h1>Hier kommt eine dynamische Ausgabe!</h1>
        <?php
            $ergebnis = 4 + 4;
            echo "<p>4 + 4 = $ergebnis</p>";
        ?>
    </body>
</html>
```

Die obige Datei sieht auf den ersten Blick wie eine normale HTML-Seite aus. Interessant wird es in den Zeilen 9 bis 12.
Dort ist in den Tag `<?php ?>` der eigentliche PHP-Quelltext eingebettet. Wenn die Seite vom Webserver angefordert wird,
wertet zunächst ein PHP-Interpreter diesen Quelltext aus und ersetzt ihn durch die Ausgaben des Skripts. Das Ergebnis
sieht folgendermaßen aus:

```html
<html>
    <head>
        <title>PHP-Test</title>
    </head>
    <body>
        <h1>Hier kommt eine dynamische Ausgabe!</h1>
        <p>4 + 4 = 8</p>
    </body>
</html>
```

## Wie funktioniert PHP?

1. Der Webbrowser fragt beim Webserver nach der Datei `/vierplusvier.php`.
1. Der Webserver liest die angefragte Datei von der Festplatte.
1. Der Webserver sendet die Datei an den PHP-Interpreter.
1. Der PHP-Interpreter führt den Code aus und liefert den erzeugten HTML-Quelltext an den Webserver zurück.
1. Der Webserver sendet den erzeugten HTML-Quelltext an den Webbrowser. Dieser wertet den HTML-Quelltext aus und zeigt
   das Ergebnis an.

## PHP Basics

### Installation

Die Installation von PHP ist abhängig von Betriebssystem und Webserver, sowie dem gewünschten Operationsmodus.
In der Berufsschule bauen die Beispiele auf `XAMPP` auf, was kurz ist für Apache, MariaDB, PHP & Perl.

Für die genaue Anleitung möchte ich euch bitte auf die
[Dokumentation/Website](https://www.apachefriends.org/de/index.html) verweisen.

### Erste Schritte

Nachdem wir von den obigen Abschnitten bereits wissen das wir für PHP einen Webserver und den PHP Interpreter als
Minimum benötigen und auch schon wissen wie wir PHP in ein HTML Dokument einbauen können wir eigentlich schon loslegen.

Wichtig zu wissen ist, dass ein Webserver in der Regel ein im Hintergrund laufender Prozess. Dieser Prozess muss
entweder beim starten des Rechners automatisch gestartet werden oder vom Benutzer gestartet werden. Ist dies nicht der
Fall, so ist die PHP Datei nicht mehr als eine Textdatei. Der Webserver muss zuvor auch für die Benutzung von PHP
konfiguriert werden. 

### Inhalte ausgeben

Möchte man aus seinem PHP-Skript etwas ausgeben, so tut man dies sauber mit `echo "Text"`. Hierbei kann wie oben im
Beispiel zu sehen natürlich auch HTML Code mit ausgegeben werden. Die unsaubere Variante ist einfach eine Variable
ohne jeglichen Kontext in den Quellcode zu packen. Zumeist wird der Inhalt dann einfach ausgegeben.

Beim Ausgeben der Inhalte ist zu beachten das `echo` immer Texte ausgeben möchte. Heißt das auszugebende Objekt muss ein
Text sein. Für Zahlen ist PHP so gutmütig und konvertiert diese automatisch in Text. Für komplexere Objekte und Arrays
muss eine explizite Konvertierung in Text vorgenommen werden, damit diese korrekt dargestellt werden können.

### Besondere Variablen

In PHP gibt es das Konzept von Superglobalen Variablen. Diese sind die folgenden nach aktuellem Stand (2020-09-28):

- `$GLOBALS`
- `$_SERVER`
- `$_GET`
- `$_POST`
- ...

Eine aktuelle Auflistung gibt es immer hier: <https://www.php.net/manual/de/language.variables.superglobals.php>

Diese erfüllen alle einen Zweck und sind vorhanden im korrekten Kontext. Sie müssen nicht vom Programmierer befüllt
werden. Wichtig ist zu wissen wie sie heißen, damit Namenskonflikte verhindert werden.

### Verzweigungen, Schleifen und Logische Operationen

Im folgenden eine Auflistung der Standardkonzepte von PHP. Erklärungen zu den jeweiligen Konstrukten sind genügend im
Netz zu finden bzw. in früheren Posts von AWP mit der Sprache C++.

If-Else:

```php
if ($a > $b) {
    echo "a ist größer als b";
} elseif ($a == $b) {
    echo "a ist gleich groß wie b";
} else {
    echo "a ist kleiner als b";
}
```

Und für unsere Schlangenverrückten:

```php
if ($a > $b):
    echo $a." ist größer als ".$b;
elseif ($a == $b): // elseif in einem Wort!
    echo $a." ist gleich groß wie ".$b;
else:
    echo $a." ist weder größer als noch gleich wie ".$b;
endif;
```

Switch-Case:

```php
switch ($i) {
    case 0:
        echo "i ist gleich 0";
        break;
    case 1:
        echo "i ist gleich 1";
        break;
    case 2:
        echo "i ist gleich 2";
        break;
    default:
           echo "i ist nicht gleich 0, 1 oder 2";
}
```

Für unsere Python verrückten:

```php
switch ($i):
    case 0:
        echo "i ist gleich 0";
        break;
    case 1:
        echo "i ist gleich 1";
        break;
    case 2:
        echo "i ist gleich 2";
        break;
    default:
        echo "i ist nicht gleich 0, 1 oder 2";
endswitch;
```

For:

```php
for ($i = 1; $i <= 10; $i++) {
    echo $i;
}
```

```php
$arr = array(1, 2, 3, 4);
foreach ($arr as &$value) {
    $value = $value * 2;
}
```

While-Do:

```php
$i = 1;
while ($i <= 10) {
    echo $i++;
}
```

Do-While:

```php
$i = 0;
do {
    echo $i;
} while ($i > 0);
```

Eine aktuelle Liste gibt es immer zu finden auf: <https://www.php.net/manual/de/language.control-structures.php>
