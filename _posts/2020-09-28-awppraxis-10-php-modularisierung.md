---
layout: post
title: "10. PHP-Modularisierung"
date: 2020-03-29 10:10:00 +0200
category: 2. Lehrjahr
tags: [Anwendungsprogrammierung, Praxis]
---

## PHP Funktionen

Das folgende Stück aus Pseudocode zeigt eine allgemeine PHP Funktion. Die Parameterliste ist mit einem Komma getrennt.

```php
function Funktionsname(Parameterliste) {
    //Anweisungen
}
```

Hier nun ein explizites Beispiel einer Funktion welche zwei Parameter entgegen nimmt und diese ausgibt miteinander
verkettet.

```php
function test($Wort1, $Wort2) {
    echo $Wort1 . $Wort2;
}
```

Die über diesem Absatz definierte Funktion wird nun im Folgenden aufgerufen:

```php
test("Hallo", " Welt");
```

Das folgende Beispiel definiert nun eine Funktion mit Rückgabewert. In diesem Fall wird lediglich der Paramter welcher
der Funktion übergeben wird zurückgegeben.

```php
function test($Wert) {
    return $Wert;
}
echo test("Hallo");
```

Dieses Beispiel ist nun ein klein wenig komplizierter und verbindet Funktionsargument, Verarbeitung in der Funktion und
Rückgabe mit der Ausgabe des zurück gegebenen Wertes. Es verdeutlicht auch das Prinzip "Call-By-Value".

```php
function test($Wert) {
    $Wert++;
    return $Wert;
}
$Zahl=1;
echo $Zahl . "/" . test($Zahl) . "/" . $Zahl;

```

Dieses Beispiel ist nun ein klein wenig komplizierter und verbindet Funktionsargument, Verarbeitung in der Funktion und
Rückgabe mit der Ausgabe des zurück gegebenen Wertes. Es verdeutlicht auch das Prinzip "Call-By-Reference".

```php
function test(&$Wert) {
    $Wert++;
    return $Wert;
}
$Zahl=1;
echo $Zahl . "/" . test($Zahl) . "/" . $Zahl;

```

Dieses Beispiel soll nun verdeutlichen das es verschiedene Gültigkeitsbereiche in PHP gibt. Die Namensgebung einer
Variable muss lediglich im selben Bereich eindeutig sein.

```php
$Zahl=1;
function test() {
    $Zahl=2;
    return $Zahl;
}
echo $Zahl . "/" . test() . "/" . $Zahl;

```

Das Schlüsselwort `global` sucht nach einer Variable mit der Bezeichnung, die darauf folgt und macht sie für den lokalen
Bereich verfügbar. Dies sollte sparsam eingesetzt werden, da es einfach übersehen werden kann und auch schnell Fehler
produziert.

```php
$Zahl=1;
function test() {
    global $Zahl;
    $Zahl=2;
    return $Zahl;
}
echo $Zahl . "/" . test() . "/" . $Zahl;

```

## Arrays als Funktionsparameter und Rückgabewerte

```php
$arDaten = array(123, 456, 789);
function AusgabeInhalte($arWerte){
    foreach($arWerte as $Element)
        echo $Element . "<br>";
}
AusgabeInhalte($arDaten);
```

```php
function test(){
    $arDaten[] = 456;
    $arDaten[] = 123;
    $arDaten[] = 789;
    return $arDaten;
}
$arWerte = test();
// Variante 1
for($i=0; $i<count($arWerte); $i++)
    echo $arWerte[$i] . "<br>\n ";
// Variante 2
foreach ($arWerte as $Eintrag)
    echo $Eintrag . "<br>\n ";
```

## Auslagern von Quellcode - require vs include

Wenn ein Projekt eine gewisse Größe erreicht ist es nicht mehr sinnvoll irgendwann alles in einer Datei zu pflegen.
Der PHP Interpreter liest aber normalerweise trotzdem nur die gerade angefragte Datei des Webservers. Um dieses Problem
zu lösen, gibt es die Schlüsselwörter `require` und `include`. Beide Schlüsselwörter laden an der Stelle wo sie stehen
eine weitere angegebene PHP Datei herein und interpretieren diese, nachdem die Interpretation abgeschlossen ist, wird
die Interpretation der "übergeordneten" Datei fortgesetzt.

Wichtig zu wissen ist folgender Effekt: Wir haben die folgenden beiden Dateien

`main.php`

```php
<?php
    require ("Funktionen.inc.php");
    Ausgabe();
?>
```

`Funktionen.inc.php`

```php
<?php
    function Ausgabe(){
        echo "externes Hallo";
    }
    echo "Jetzt kommt ein <br>";
?>
```

Das Ergebnis ist nun folgendes nach dem Interpretieren der Datei:

```php
<?php
    function Ausgabe(){
        echo "externes Hallo";
    }
    echo "Jetzt kommt ein <br>";
    Ausgabe();
?>
```

Was bedeutet das nach dem Ausführen der vollständigen PHP-Datei das ganze so aussieht:

```html
Jetzt kommt ein <br>
externes Hallo
``` 

Wir hätten obigen Code natürlich auch statt `require("Funktionen.inc.php)` mit `include("Funktionen.inc.php)` schreiben
können. Warum haben wir das nicht?

Der Unterschied von `require` vs `include` ist das Verhalten wenn die Datei `Funktionen.inc.php` nicht gefunden werden
kann. `require` gibt an der Stelle einen fatalen Fehler aus und beendet die Ausführung des Skriptes, wohingegen
`include` lediglich eine Warnung ausgibt und anschließend mit der Ausführung des Skriptes fortführt. Je nach
Anwendungsfall kann es sein das das eine oder andere Schlüsselwort sinnvoller ist oder eben nicht.
 