---
layout: post
title: "03. Kalkulatorische Abschreibung"
date: 2020-11-28 09:50:17 +0200
category: 3. Lehrjahr
tags: [ Betriebswirtschaftliche Prozesse]
---

## Fallbeispiel - Abschreibungen

Spezialdrucker

- Anschaffungskosten				2.400,00
- Nutzungsdauer lt. Abschreibuntgstabelle		3 Jahre

| Verteilung des Anschaffungswertes auf die Nutzungsjahre (2.400,00 Euro /3 = 800,00 Euro) | Abschreibung pro Jahr | Restwert |
| --------------- | ------ | -------- |
| nach dem 1 Jahr | 800,00 | 1.600,00 |
| nach dem 2 Jahr | 800,00 | 800,00   |
| nach dem 3 Jahr | 800,00 | 0,00     |

Verbuchung als Aufwand führt zur Minderung des zu versteuernden Gewinns um 800,00 Euro im jeweiligen Jahr.

Problem:

- Der Drucker wird in der Realität 8 Jahre genutzt.
- Der Wiederbeschaffungswert würde mittlerweile 2.700,00 Euro betragen.

Lösung:

Bilanzielle Abschreibung = Anschaffungskosten (AK) / Nutzungsdauer (ND) = 2400/3 = 800€

$$\rightarrow$$ ND vom Gesetzgeber vorgeschrieben

Kalkulatorische Abschreibung = Wiederbeschaffungswert (WBW) / tatsächliche Nutzungsdauer = 2700/8 = 337,50

$$\rightarrow$$ Versuch, realistischer Werte abzubilden

Eintragung in die Ergebnistabelle

| Rechnungskreis I - Geschäftsbuchführung | | | Rechnungskreis II - Kosten- und Leistungsrechnung (KLR) | | | | | |
| --- | --- | --- | --- | --- | --- | --- | --- |  ---  |
| Erfolgsbereich | | | Abgrenzungsbereich | | | | KLR-Bereich | |
| | GuV-Rechnung | | Unternehmensbezogene Abgrenzungen | | Kosten und Leistungsrechnerische Korrekturen | | | |
| Kontenbezeichnung | Aufwand | Ertrag | Neutr. Aufwand | Neutr. Ertrag | Aufwand lt. GB | Erträge | Kosten | Leistungen |
| | | | | | | | | |
| Abschreibung | 800,00 | | | | 800,00 | 337,50 | 337,50 | |

Merke: kalkulatorische Abschreibungen stellen Anderskosten da, da hier in der Kosten- und Leistungsrechnug ein anderer
Betrag erfasst wird, als in der Geschäftsbuchführung offiziell gebucht werden kann.

## Fallbeispiel - Zinsen

Das Unternehmen A finanziert einen neuen Server per Kredit und zahlt im Jahr 2019 dafür
600,00 Euro Zinsen. Diese werden als Aufwendungen verbucht und mindern den zu
versteuernden Gewinn.

Bsp. Gewinn- und Verlustkonto

![BWP - Kalk Abschreibung - Fallbeispiel Zinsen](/assets/img/BWP/03-kalk-abschreibung.png)

Problem
Das Unternehmen B finanziert den neuen Server komplett aus den im letzten Quartal
erwirtschafteten Gewinnen.

Fazit

- Ungerecht: bei Finanzierung des Servers über Eigenkapital keine Buchung der Zinsaufwendungen möglich
  $$\rightarrow$$ Gewinn kann so nicht verschmälert werden und es müssen mehr Steuern bezahlt werden.

Annahme:
- Betriebsnotwendiges Kapital müsste komplett fremdfinanziert werden
- Zum landesüblichen Zinssatz $$\rightarrow$$ kalkulatorische Zinsen

> Die kalkulatorische Zinsen stellen Anderskosten dar.

Eintragung in die Ergebnistabelle

| Rechnungskreis I - Geschäftsbuchführung | | | Rechnungskreis II - Kosten- und Leistungsrechnung (KLR) | | | | | |
| --- | --- | --- | --- | --- | --- | --- | --- |  ---  |
| Erfolgsbereich | | | Abgrenzungsbereich | | | | KLR-Bereich | |
| | GuV-Rechnung | | Unternehmensbezogene Abgrenzungen | | Kosten und Leistungsrechnerische Korrekturen | | | |
| Kontenbezeichnung | Aufwand | Ertrag | Neutr. Aufwand | Neutr. Ertrag | Aufwand lt. GB | Erträge | Kosten | Leistungen |
| | | | | | | | | |
| Zinsaufwendungen | 600,00 (Zinsen lt. GB) | | | | 600,00 (Zinsen lt. GB) | 5000,00 (kalk. Zinsen) | 5.000 (kalk. Zinsen)| |

## Fallbeispiel - Unternehmerlohn

Bei Kapitalgesellschaften erhalten die Vorstandsmitglieder von Aktiengesellschaften bzw. Geschäftsführer von GmbHs für
ihre leitende Tätigkeit Gehälter, die in der KLR eingehen. Unternehmer, die in Einzelunternehmungen (e. K.) und
Personengesellschaften (OHG, KG) leitend tätig sind, dürfen dagegen aus steuerrechtlichen Gründen keine Gehälter
beziehen. Ihre Lebensführungskosten decken sie durch Privatentnahmen.

In der KLR müssen alle Kosten berücksichtigt werden, die aus dem leistungsbedingten Verzehr von Sachgütern und
Dienstleistungen resultieren. Hierzu gehören auch die dispositive Arbeit des Unternehmers in Einzelunternehmungen und
Personengesellschaften. Die planenden und leitenden Arbeitsleistungen des Unternehmers werden deshalb als
kalkulatorische Zusatzkosten in die KLR einbezogen.

Die Höhe des kalkulatorischen Unternehmerlohns bemisst sich am Gehalt eines mit einer vergleichenden Tätigkeit
beauftragten leitenden Angestellten.

Beispiel: Der kalkulatorische Unternehmerlohn einer OHG beträgt im Mai 20.000,00 EUR. Der kalkulatorische
Unternehmerlohn findet folgendermaßen in der Ergebnistabelle Berücksichtigung:

| Rechnungskreis I - Geschäftsbuchführung | | | Rechnungskreis II - Kosten- und Leistungsrechnung (KLR) | | | | | |
| --- | --- | --- | --- | --- | --- | --- | --- |  ---  |
| Erfolgsbereich | | | Abgrenzungsbereich | | | | KLR-Bereich | |
| | GuV-Rechnung | | Unternehmensbezogene Abgrenzungen | | Kosten und Leistungsrechnerische Korrekturen | | | |
| Kontenbezeichnung | Aufwand | Ertrag | Neutr. Aufwand | Neutr. Ertrag | Aufwand lt. GB | Erträge | Kosten | Leistungen |
| | | | | | | | | |
| ... | ... | | | | | 20000,00 (kalk. U-Lohn) | 20000,00 (kalk. U-Lohn) | |

> Der kalkulatorische Unternehmerlohn stellt Zusatzkosten dar, da ihm in der Geschäftsbuchführung keine
> Aufwandsbuchungen vorausgehen.

## Fallbeispiel - Miete

Bei Unternehmen die eigene Geschäfts-, Lager-, und Fabrikgebäude besitzen, fallen statt Miet-
zahlungen folgende Aufwendungen an:

$$\rightarrow$$ Abschreibungen auf Gebäude, Aufwendungen für Gebäudereparaturen, Grundsteuerzahlungen, Hypothekenzinsen,
Versicherungsprämien, Kaminkehrgebühren, Müllabfuhrgebühren, Straßenreinigungsgebühren usw.

In die KLR sollte aber ein Mietwert eingehen, der für vergleichsweise gemietete Gebäude hätte bezahlt werden müssen.

Da nun aber die wesentlichen Bestandteile der Gebäudekosten, nämlich die Gebäudeabschreibungen durch die
kalkulatorischen Abschreibungen und die Hypothekenzinsen durch die kalkulatorischen Zinsen bereits in die KLR
eingeflossen sind.

In den meisten Betrieben, die eigene Geschäfts-, Lager- und Fabrikgebäude besitzen, wird zur Vereinfachung auf die
Erfassung einer kalkulatorischen Miete verzichtet.

> Die kalkulatorische Miete stellt Anderskosten dar.

## Fallbeispiel - Unternehmerische Wagnisse

Das allgemeine Unternehmerwagnis, das sich z. B. aus Verlusten infolge eines konjunkturell bedingten Absatzrückgangs
etc. ergeben kann, wird durch den Gewinn abgegolten und kann daher in der KLR nicht berücksichtigt werden.

Im Gegensatz zum allgemeinen Unternehmerwagnis können besonders betriebsbedingte Einzelwagnisse wie z. B. das
Beständewagnis, Anlagewagnis, Gewährleistungswagnis etc. in die KLR einbezogen werden, soweit sie nicht durch
Fremdversicherung abgedeckt sind.

Die tatsächlichen eingetretenen Wagnisverluste werden in der Geschäftsbuchführung erfasst. Sie fallen in schwankenden
Höhen und in unregelmäßigen Zeitabständen an und sind somit unter dem Gesichtspunkt der Stetigkeit des Kostenansatzes
für die KLR unbrauchbar.

Um eine gewisse Konstanz beim Kostenansatz in der KLR zu erreichen, werden hier für die kalkulatorischen Wagnisse
Durchschnittswerte angesetzt. Diese werden aufgrund von tatsächlich in den vorausgegangenen Jahren eingetretenen
Wagnisverlusten errechnet.

Beispiel: In den letzten 6 Jahren beliefen sich die Gewährleistungskosten eines Onlinehändlers auf 1 % vom Umsatz. Der
Umsatz des kommenden Geschäftsjahres wird auf 800.000,00 € geschätzt. Das heißt es werden 8.000,00 € als
Gewährleistungswagnis für das kommende Geschäftsjahr angesetzt.

> Die kalkulatorischen Wagnisse stellen Anderskosten dar.

In der Ergebnistabelle werden die kalkulatorischen Wagnisse wie die übrigen Anderskosten behandelt.

