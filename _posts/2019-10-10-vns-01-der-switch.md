---
layout: post
title:  "01. Der Switch"
date:   2019-10-07 13:30:00 +0100
category: 2. Lehrjahr
tags: [Vernetzte Systeme]
---

## Auswahlkriterien beim Kauf eines Switches

| Auswahlkriterium                | Eigenschaft                                   |
|---------------------------------|-----------------------------------------------|
| Übertragungsgeschwindigkeit     | 10 Mbit/s - 10 Gbit/s                         |
| Anzahl der Anschlüsse/Ports     | 2 … 128                                       |
| Größe/Maß                       | 19''/30cm/etc.                                |
| Stromverbrauch/Energieverbrauch | Bis 50W                                       |
| Buchsen/Anschlüsse              | RJ45/GG45/SPF+                                |
| Features                        | PoE, Remote Management, VLAN, Redundanz, SNMP |
| Hersteller                      | 3Com/Cisco/Netgear                            |
| Kosten                          | 5€…100000€                                    |
| Garantie/Lieferbarkeit/Service  |                                               |

## Exkurs: Flow Control & Back-Pressure - wichtige Funktionalitäten eines Switches

### Ethernet flow control

Ethernet flow control is a mechanism for temporarily stopping the transmission of data on an Ethernet computer network.
Flow control in Ethernet resides on the data link layer. A situation may arise where a sending station (computer) may be
transmitting data faster than some other part of the network (including the receiving station) can accept it. The
overwhelmed network element will send a _PAUSE_ frame, which halts the transmission of the sender for a specified period
of time. _PAUSE_ is a flow control mechanism on full duplex Ethernet link segements defined by IEEEE 802.3x and uses MAC
Control frames to carry the PAUSE commands. Only station configured for full-duplex operation may send _PAUSE_ frames.
(From Wikipedia, the free encyclopedia, angepasst)

### Back-Pressure

Ist kein Vollduplex möglich, wird ein Verfahren namens Back-Pressure verwendet. Es simuliert Kollisionen. Dazu wird vor
dem drohenden Überlauf ein JAM-Signal vom Switch gesendet. Das angeschlossene Gerät beendet daraufhin den Sendevorgang
und wartet einige Zeit, bevor es erneut Frames sendet. (nach Schnabel, P.: Netzwerktechnik-Fibel, 2004)

## CSMA/CD und verwandte Techniken

CSMA/CD ist ein wesentlicher Bestandteil der Netzwerkarchitektur Ethernet und regelt den Zugriff auf das Medium.

Sobald sich mehrere Kommunikationsteilnehmer ein einziges Medium zur Datenübertragung teilen (Shared Media), muss ein
Verfahren definiert werden, welches den Zugriff regelt. Ansonsten kommt es zu Kollisionen.

Carrier Sense Multiple Access / Collision Detection ist ein solches Verfahren und im Standard IEEE802.3 definiert.
Seinen Ursprung hat CSMA/CD in Netzwerken mit logischer Bustopologie.

Funktionsprinzip:

- Bevor ein Teilnehmer Daten sendet, muss die Leitung abgehört und sichergestellt werden, dass diese frei ist.
- Ist die Leitung nicht frei, wird gewartet und das Medium wird weiter geprüft.
- Bei freier Leitung kann der Teilnehmer senden; die Leitung wird dabei weiter überwacht und abgehört.
- Sollten zwei oder mehrere Teilnehmer gleichzeitig Signale auf das Medium geben, kommt es zu Kollisionen. In diesem
  Fall, sendet die festellende Station ein JAM-Signal, um alle angeschlossenen Systeme zu informieren.
- Das Senden wird eingestellt, das Verfahren beginnt von neuem...

{% mermaid %}
graph LR
    id1[Station sendebereit]
    id2[Leitung abhören]
    id3{frei?}
    id4[Daten senden und abhören]
    id5{Kollision?}
    id6[Warten gemäß Back-Off-Strategie]
    id7[stop Übertragung]
    id8[JAM Signal]
    id9(Fertig)
    
    id1 --> id2
    id2 --> id3
    id3 -->|Nein|id2
    id3 -->|Ja|id4
    id4 --> id5
    id8 --> id7
    id7 --> id6
    id5 -->|Nein|id9
    id5 -->|Ja|id8
{% endmermaid %}

## Funktions- und Arbeitsweise von Ethernetswitches

Bevor wir uns mit der Funktions und Arbeitsweise von Switches befassen können müssen wir uns erst einmal damit
auseinandersetzen, was "_Ethernet_" überhaupt genau ist. Dies ist im IEEE Standard 802.3 beschrieben.

Eine Übertragung im lokalen Ethernet besteht aus sogenannten _Rahmen_ bzw. engl. _Frames_. Eine Erklärung hierfür ist
untenstehend nun zu lesen nach "Introduction to Networks, Cisco Networking Academy-2014"

> Ethernet is the most widely used LAN technologiy today. Ethernet operates in the data link layer and the physical
> layer. It is a family of networking technologies that are defined in the IEEE 802.2 and 802.3 standards. Ethernet
> supports data bandwidths of: 10 Mb/s, 100 Mb/s, 1000 Mb/s (1 Gb/s), 10.000 Mb/s (10 Gb/s), 40 Gb/s, 100 Gb/s
>
> Traditionally, Ethernet provides unacknowledged connectionless service over a shared media using CSMA/CD as the media
> access methods. Shared media requires that the Ethernet frame header use a address to identify the source and
> destination nodes. As with most LAN protocols, this address referred to as the MAC address of the node. An Ethernet
> MAC address is 48 bits and is generally represented in hexadecimal format (64-31-50-3F-70-C3).
>
> Fields of the Ethernet frame:
> - Preamble: Used to time synchronization; this also contains a delimiter to mark the end of timing information
> - Destination Address: 48-bit MAC address for the destination node
> - Source Address: 49-bit MAC address for the source node
> - Type: Value to indicate which upper-layer protocol will receive the data after the Ethernet process is complete
> - Data or Payload: This is the PDU (Protocol Data Unit), typically an IPv4 packet, that is to be transported over the
>                    media
> - Frame Check Sequence (FCS): A CRC value used to check for damaged frames
