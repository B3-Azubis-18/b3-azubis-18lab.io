---
layout: post
title:  "03. Business Language"
date:   2019-02-14 11:15:00 +0100
category: 1. Lehrjahr
tags: [Englisch]
--- 

## Layout of business letters

- An **opening** is the beginning of a text it tells the reader why you know his company.
- The **address of sender** is the address of the person who wants an offer.
- The **subject line** informs you about the purpose of a letter.
- The **salutation** is the greeting of the reader.
- The **main part** you are telling the reader what you want.
- The **address of receiver** is the address of the person to whom the letter is sent.
- The **ending** is a request for an answer.
- The **close** is a formal way to say goodbye to the reader.
- The **date** is obviously the date of writing.

## Enquiry

An enquiry consists of the same elements as a normal business letter. There are no additional elements.

### Structure of an enquiry

Introduction

- Source of information (Where did we get the supplier's name and address from?)
- Introduce your company ("We are a ... company...")

Main part

- Specific enquiry if you want to know more about a specific product or service
  1. Type and quality of goods
  2. Quantity
  3. Price
  4. Terms of payment
  5. Terms of delivery
  6. Delivery data
- General enquiry if you ask generally about products a company offers, prices, terms of payment and delivery
  1. Catalogues
  2. Samples
  3. Further information
  4. Discounts (if you intend to place a big order)

Close

- Hope for your prompt reply

### Intentions

Enquiries are made by a buyer in order to find out who is the most favourable supplier. They can be either general —
i.e. asking only for a catalogue — or they are specific inquiries which ask for a certain type and quality of goods,
the price per item, terms of payment and delivery and the earliest possible delivery date. Buyers often expect the
supplier to be able to deliver ex stock. Sources of information about goods can be the Chamber of Commerce,
advertisments in newspapers and magazines, trade fairs, or business partners who recommend something to you. If the
quality is very important you might expect to receive a sample of the product.

### Salutations and closings

| Person addressed | Salutations | Closings |
| ---------------- | ----------- | -------- |
| Mr Jones         |             |          |
| Dr. A. Brown.    |             |          |
| Judith Smith     |             |          |
| the Manager      |             |          |
| any firm (US)    |             |          |
| any firm (GB)    |             |          |

### Example

TODO

## Offer

An enquiry shows interest in your company and a personal well-rounded reply may be the beginning of a long term business
relationship. Personalize the salutation whenever possible by using the addressee's name. Your reply should begin by
thanking the enquirer for his/her interest. The offer should inform a prospective customer about your products or
services and terms. A good offer not only answers all the customer's questions — it often goes beyond the original
enquiry. It includes a description of the enquired goods, including information on prices, discounts, terms of delivery
and payment and delivery period. The customer must also be informed about the validity of the offer. End your
communication with a phrase designed to create good will. Offers are binding on the person or firm making the offer.
