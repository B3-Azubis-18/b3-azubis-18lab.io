---
layout: post
title:  "03. Erweiterte Switch Funktionalitäten"
date:   2019-10-14 10:15:00 +0100
category: 2. Lehrjahr
tags: [Vernetzte Systeme]
---

Switching-Technologien haben sich über die letzten Jahre weiter entwickelt. Hohe Datendurchsatz und zusätzliche
Funktionen machen den Switch zum zentralen Gerät lokaler Netzwerke.

Neben reinen layer-2-Geräten gibt es auch zunehmend layer-3-Switches, die Adressinformationen der nächst höheren
Protokollebene auswerten und für die Wegwahl nutzen können. Der Name Switch ist für solch ein Gerät leicht irreführend,
da es sich auf der dritten OSI·Ebene um Routing handelt. In der Regel unterstützen layer-3-Switches IP-basiertes
Routing, wobei sie Protokolle wie RIP (Routing Information Protocol), RIPv2 und OSPF (Open Shortest Path First) nutzen.
Herkömmliche Router haben gegenüber Switches eine höhere Latenz. Leistungsfähige Layer-3-Switches erledigen hingegen
das Routing mit Wire-Speed, weshalb sie traditionellen Routern inzwischen den Rang ablaufen.

Die nächste Steigerung sind Layer-4- bis Layer-7-Switches, die für die Wegwahl Informationen der höheren OSI-Schichten
nutzen (Content-Switching). So werten Layer-4-Switches beispielsweise fest definierte Ports wie Port 80 (HTTP-Verkehr)
aus und leiten Daten dafür an einen Webserver weiter. Layer-7-Switches gehen noch einen Schritt weiter und werten
Anwenderinformationen aus, indem sie nach Schlüsselinformationen in den Nutzdaten suchen.

## Virtuell geschaltet

Die meisten managebaren Switches bieten Virtual (Bridged) Local Area Networks, kurz VLANs, an. Diese stellen, auf der
MAC-Ebene implementiert, eine Gruppierung von bestimmten Stationen innerhalb des gesamten LANs dar. Der Vorteil: Das
Netzwerk wird in mehrere Broadcast-Domänen aufgeteilt. Außerdem kann man die Datensicherheit verbessern, da nur die
Teilnehmer eines VLAN - beispielsweise die Mitarbeiter der einzelnen Abteilungen eines großen Unternehmens -
untereinander Daten austauschen können. Für Switch-übergreifende VLANs existiert ein IEEE-Standard (802.1Q).

## Vorfahrt

High-End-Switches bieten häufig auch Quality-of-Service Funktionen (QoS), mit denen man den weiterzuleitenden Frames
unterschiedliche Prioritäten einräumen kann. Dieses Feature wird mit dem Aufkommen von Voice-over-IP (VoIP, LAN-
beziehungsweise Internet-Telefonie) und Videostreams immer wichtiger, weil vor allem Sprache gegenüber zu hohen Latenzen
sehr empfindlich ist. Mittels QoS kann man für solche Anwendungen Bandbreite reservieren und bestimmte Frames
priorisieren, was jedoch garantierte Verbindungseigenschaften erfordert.

Bei größeren LANs steht neben der Durchsatzoptimierung auch die Ausfallsicherheit auf der Agenda. Letztere kann man
durch Redundanzschaffung verbessern. Dabei verbindet man alle Switches in einem Firmen-LAN über einen oder mehrere Ringe
auf unterschiedlichen physischen Wegen, was eigentlich verboten ist. Damit in solchen Schleifen keine Frames endlos
kreisen, kommt das Spanning-Tree-Verfahren zum Einsatz. Es ist im IEEE-802.1D-Standard für die MAC-Ebene spezifiziert
und basiert auf dem Austausch von Konfigurationspaketen (Bridge Protocol Data Units, BPDU), die über Multicast Frames an
eine bestimmte Adresse (0l-80-C2-00-00-10) gehen.

## Bäume spannen

Über BPDUs nehmen die beteiligten Switches eine Topologiekontrolle vor, die redundante Strecken erkennt und zwischen
diesen die optimale Route ermittelt, wobei sie unter anderem die möglichen Datenraten und Entfernungen berücksichtigen.
Diese Parameter - wie auch Prioritäten und Wegekosten - sind normalerweise konfigurierbar, sodass der Netz-Admin
bevorzugte Routen festlegen kann. Die Ports der redundanten Strecken werden zunächst abgeschaltet und erst beim Ausfall
der Hauptroute reaktiviert. Jeder Spanning-Tree-fähige Switch muss dazu über eine eigene Bridge-1D und MAC-Adresse
verfügen.

Nachteilig am herkömmlichen Spanning Tree ist, dass es bereits in einem kleinen LAN nach einem Störfall eine halbe bis
eine Minute für die Umkonfiguration benötigt. Das in IEEE 802.1w beschriebene, abwärtskompatible Rapid Spanning Tree
(RSTP) oder Fast Spanning Tree soll den Vorgang deutlich beschleunigen: Man verspricht sich davon eine Umkonfiguration
innerhalb einer Sekunde nach dem Störfall. Einfache Switches ohne Spanning Tree besitzen stattdessen häufig eine Loop-
Detection-Funktion. Sie soll eine Schleifenbildung erkennen und löst oft einen Admineinsatz aus.

## Mehrspurverkehr

Mit Spanning Tree verwandt ist die Link Aggregation (IEEE 802.3ad). Dabei bündelt man im Punkt-zu-Punkt-Betrieb - also
beispielsweise zwischen zwei Switches oder Switch und Server mehrere Leitungen, um die Bandbreite zu erhöhen oder
Redundanz zu schaffen. Das funktioniert grundsätzlich nur bei Ports mit derselben Datenrate und im Vollduplexbetrieb.
Die zusammengefassten Anschlüsse bilden logisch betrachtet einen einzelnen Port mit vervielfachter Bandbreite. Fällt nun
eine der Strecken aus, kann der Datenverkehr über die restlichen Leitungen weiterlaufen. Anfangs wurde diese
Parallelschaltung auch als Trunking oder Channel Bundling bezeichnet, wobei die Hersteller unterschiedliche
Lösungsansätze implementierten, die zueinander inkompatibel waren. Mit der Verabschiedung des IEEE-Standards entstand
eine einheitliche Lösung unter dem Begriff Link Aggregation.
