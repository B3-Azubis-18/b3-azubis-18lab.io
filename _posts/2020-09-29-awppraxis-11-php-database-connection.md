---
layout: post
title: "11. PHP Datenbank Verbindung"
date: 2020-03-29 10:20:00 +0200
category: 2. Lehrjahr
tags: [Anwendungsprogrammierung, Praxis]
---

In PHP existieren drei Varianten um auf eine MySQL Datenbank zuzugreifen.

- MySQL: Seit PHP7 nicht mehr möglich
- MySQLi (MySQL Improved Extension): Seit PHP 5
- PDO (PHP Data Objects): Aktuellste Schnittstelle und unabhängig von dem verwendeten Datenbanksystem. Der Zugriff erfolgt allerdings ausschließlich objektorientiert und ist daher für AWP11 Unterricht nicht geeignet.

## MySQLi Grundlagen

### Verbindung herstellen

```php
$hostname = '127.0.0.1';
$username = 'Paul';
$password = '';
$databasename = 'meineDatenbank';

$Con = mysqli_connect($hostname, $username, $password, $databasename);
```

Prüfung ob Verbindung erfolgreich hergestellt wurde `die()` und `exit()` sind dabei synonym verwendbar.

```php
if(!$Con){
    exit("Datenbank Verbindungsfehler: " . mysqli_connect_error());
}
```

Charset festlegen damit Umlaute z. B. bei "Zutritt gewährt" korrekt in die DB geschrieben werden.

```php
mysqli_set_charset($Con, "utf8");
```

### SQL Query-String erstellen

Insbesondere komplexe SQL Statements sollten zuvor in einer Variable gespeichert werden. Dies sorgt für einen übersichtlicheren Quellcode.

```php
$Query = "INSERT INTO Kunden(K_KName, K_KStrasse) VALUES('";
$Query .= $Name."','";
$Query .= $Strasse."')";
```

Oder:

```php
$Update = "UPDATE user SET email = $EMail WHERE id = $UserID";
```

Via `var_dump($Query)` lässt sich das SQL Statement auf Fehler überprüfen.
Sie sollten niemals direkt `$_POST` Inhalte an den Datenbankserver schicken. In dem oberen Beispiel ist davon auszugehen, dass `$Name` und `$Strasse` bereits auf Schadcode geprüft wurde. Ansonsten besteht die Gefahr einer SQL Injection. Die Verwendung von Prepare-Statements ist zu empfehlen. Nähere Informationen hierzu unter: php-einfach MySQLi Crashkurs
SQL Query-String absenden

### DQL Abfragen

```php
$Result = mysqli_query($Con, $Query);
```

`$Result` enthält eine Menge von Datensätzen (= Recordset) oder im Falle eines Fehlers den Wert `False`. Die Datensätze könnten beispielsweise folgendermaßen ausgegeben werden:

```php
if ($Result != false) {
	foreach ( $Result as $Data) {
		echo $Data["Name"]. " - ";
		echo $Data["Hund"]. " - <br>";
	}
}
```

### DDL/DML Statements

```php
mysqli_query($Con, $Update);
```

Auch bei DDL/DML Abfragen ist zu prüfen, ob das Statement erfolgreich ausgeführt wurde. Zum Beispiel so:

```php
if (mysqli_query($Con, $Update) == false) {
	echo "Update-Fehler: " . mysqli_error($Con);
};
```

### Trennen der Datenbankverbindung

Je hergestellte Verbindung soll immer sauber getrennt werden, nachdem sie nicht mehr benötigt wird.

```php
mysqli_close($Con);  
```
