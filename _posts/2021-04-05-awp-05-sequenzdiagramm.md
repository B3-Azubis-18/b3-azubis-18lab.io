---
layout: post
title: "05. Sequenzdiagramm"
date: 2021-01-20 08:30:00 +0200
category: 3. Lehrjahr
tags: [Anwendungsprogrammierung]
---

## Was ist ein Sequenzdiagramm?

- Ein Sequenzdiagramm zeigt Objekte, die eine Reihe von Nachrichten in einer zeitlich begrenzten Situation austauschen,
  wobei der zeitliche Ablauf betont wird
- Nachricht $$\rightarrow$$ Aktivierung der Methode gleichen Namens
- 2 Dimensionen:
  - Vertikale: Zeit
  - Horizontale: Objekte
- Jedes Objekt hat eine senkrechte Lebenslinie
- Ein Balken auf der Lebenslinie zeigt Aktivität an
- Nachrichten sind Pfeile, Antworten gestrichelte Pfeile

> Sequenzdiagramme verwendet man zur Darstellung des zeitlich geordneten Nachrichtenflusses zwischen Objekten

## Notationselemente

### Objektsymbole

Am oberen Ende des Diagramms werden Objektsymbole angetragen.

Bei diesen Objekten handelt es sich nicht um spezielle Objekte, sondern um Stellvertreter für beliebige Objekte der
angegebenen Klasse.

Daher werden sie als anonyme Objekte (d.h. `:Klasse`) bezeichnet.

Die Reihenfolge der Objekte ist beliebig, sie soll so gewählt werden, dass ein übersichtliches Diagramm entsteht.

![AWP - Sequenzdiagramm - 01](/assets/img/AWP/05-sequenzdiagramm-01.png)

### Lebenslinie / Objektlinie

Jedes Objekt wird durch eine gestrichelte Linie im Diagramm dargestellt.

Die Linie beginnt nach dem Erzeugen des Objektes und endet mit dem Löschen. Existiert das Objekt während der gesamten
Ausführungszeit des Szenarios, so ist die Linie von oben nach unten durchgezogen.

Das Rechteck, d.h. die "Verdickung der Linie" zeigt an, wann das Objekt aktiv ist.

X Markiert das Löschen eines Objektes

![AWP - Sequenzdiagramm - 02](/assets/img/AWP/05-sequenzdiagramm-02.png)

### Botschaften / Nachrichten

Dienen zum Aktivieren der Operationen (Methodenaufrufe). Jede Botschaft wird als Pfeil vom Sender zum Empfänger
gezeichnet.

Der Pfeil wird mit dem Namen der aktivierten Operation (Methode) beschriftet. Die Nachricht kann synchron
($$\rightarrow$$ - Pfeilspitze sollte gefüllt sein; Sender gesperrt, wartet auf Antwort) oder asynchron
($$\rightarrow$$; Sender nicht gesperrt) sein.

Nach Beenden einer synchronen Operation kann ein gestrichelter Pfeil zeigen, dass Kontrollfluss zur aufrufenden
Operation zurückgeht ($$\dashleftarrow$$; optional).

![AWP - Sequenzdiagramm - 03](/assets/img/AWP/05-sequenzdiagramm-03.png)

### Interaktion

### Beispiel: Schleife = Loop

Die Schleife gibt an, dass eine Nachricht mehrmals an viele Empfängerobjekte gesendet wird.

![AWP - Sequenzdiagramm - 04](/assets/img/AWP/05-sequenzdiagramm-04.png)

### Beispiel: Bedingung = Alternative

Es kann angegeben werden, dass eine Nachricht nur dann gesendet wird, wenn eine bestimmte Bedingung zutrifft.

Leere "else"-Zweige können weggelassen werden.

![AWP - Sequenzdiagramm - 05](/assets/img/AWP/05-sequenzdiagramm-05.png)

### Selbstdelegation

Ein Objekt kann u.U. Nachrichten an sich selbst senden (eigene Methoden aufrufen).

Hier ruft ein Objekt der Klasse CArtikel (z.B. nach jeder Entnahme) eine eigene Methode auf, die prüft ob ein Bestand
erreicht oder unterschritten wurde, der eine Nachbestellung erforderlich macht.

![AWP - Sequenzdiagramm - 06](/assets/img/AWP/05-sequenzdiagramm-06.png)

## Beispiel

### Praktisches Beispiel

Klassendiagramm:

![AWP - Sequenzdiagramm - 07](/assets/img/AWP/05-sequenzdiagramm-07.png)

Sequenzdiagramm für obiges Klassendiagramm:

![AWP - Sequenzdiagramm - 08](/assets/img/AWP/05-sequenzdiagramm-08.png)

### Theoretisches Beispiel mit Erläuterungen

![AWP - Sequenzdiagramm - 09](/assets/img/AWP/05-sequenzdiagramm-09.png)
