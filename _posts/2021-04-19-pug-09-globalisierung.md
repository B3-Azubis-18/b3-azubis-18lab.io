---
layout: post
title:  "09. Globalisierung"
date:   2021-04-19 09:00:00 +0100
category: 3. Lehrjahr
tags: [Politik und Gesellschaft]
---

## Allgemeines

Youtube Video zu dem Thema: <https://www.youtube.com/watch?v=8ndXSBjKO9Y>

Globalisierung ist eine politisch-ökonomische Bezeichnung für den fortschreitenden Prozess weltweiter
Arbeitsteilung.<br>
Da die politisch gesetzten Handelsschranken zwischen den Staaten zunehmend abgebaut werden und der Produktionsfaktor
Kapital weltweit mobil und einsetzbar ist und weil ferner die neuen Kommunikationstechnologien grenzenlos angewendet
werden können, wird zunehmend in solchen Staaten produziert, die die höchsten Kostenvorteile bieten. Kennzeichnend für
die G. ist, dass diese Kostenvorteile nicht nur für jedes Endprodukt (z. B. Fotokameras aus Singapur) gesucht werden,
sondern für (nahezu) jedes Einzelteil, aus dem das Endprodukt besteht (bei einem Automobil z. B. von einzelnen Schrauben
über einzelne Karosserieteile und den Motor bis zu ganzen Baugruppen etc.). Der Prozess der G. erhöht damit entscheidend
den Wettbewerbsdruck zwischen den einzelnen Unternehmen und hat darüber hinaus erhebliche Auswirkungen auf die
Stabilität und Sicherheit der Arbeitsplätze.

Quelle: <http://www.bpb.de/nachschlagen/lexika/politiklexikon/17577/globalisierung>

Ursprünglich die Bezeichnung dafür, dass die Wirtschaft heute weltweit verflochten ist, weltweite Konkurrenz und
Arbeitsteilung herrschen und Informationen dank Satellitentechnik, Fax, Laptop, Mobiltelefon und Internet nahezu
gleichzeitig an jedem Punkt der Erde zur Verfügung stehen. Inzwischen wird der Begriff allgemein für die immer stärkere
Verkoppelung von Vorgängen rund um den Globus benutzt. Was lokal irgendwo passiert, kann schnell Bedeutung für die ganze
Welt gewinnen. Globalisierungskritiker setzen sich für eine politische Regulierung ein, die den Vormarsch der Wirtschaft
zügeln soll.

Quelle: <http://www.bpb.de/nachschlagen/lexika/pocket-politik/16435/globalisierung>

Dieser Begriff kommt von "global" und das bedeutet "weltumspannend". Wahrscheinlich habt ihr schon auf unterschiedlichen
Gegenständen ein Schildchen gesehen mit der Aufschrift "Made in Taiwan" oder "Made in China". Warum kommen diese Waren
aus China und aus anderen fernen Ländern in unsere Geschäfte? Die Unternehmen, die Spielzeug oder andere Dinge
verkaufen, wollen ihre Waren so preiswert wie möglich herstellen. Sie lassen deshalb die Waren dort produzieren, wo es
nicht so viel kostet. In vielen ärmeren Ländern dieser Welt gibt es Fabriken, in denen die Menschen viel weniger Geld
für die Arbeit bekommen als in Deutschland. Und wenn deutsche Unternehmen Fabriken im Ausland bauen, müssen sie dort oft
auch weniger Steuern zahlen als hier. Dies führt dann dazu, dass die Herstellung der Waren in ärmeren Ländern billiger
ist als in Deutschland. Folglich können die Waren in Deutschland dann auch billiger verkauft werden. Durch die moderne
Computertechnologie und die Reisemöglichkeiten ist dies alles viel leichter möglich als früher, die Welt ist „näher
zusammengerückt“.

Quelle: <http://www.bpb.de/nachschlagen/lexika/das-junge-politik-lexikon/161179/globalisierung>

![Globalisierung Vor & Nachteile](/assets/img/pug/03-09-globalisierung-1.png)

## Vorteile

Vorteile der Globalisierung

Zu den Vorteilen, die jeder Bürger eines Staates genießen kann, zählt das umfangreiche Warenangebot. Ob günstige
Elektronikartikel aus China oder die Möglichkeit, ganzjährig frisches Obst aus fernen Teilen der Welt kaufen zu können
– dank der globalen Wirtschaft ist das inzwischen alltäglich. Zudem führt ein größeres Warenangebot zu sinkenden Preisen
und in Folge dessen zu einer geringeren Inflationsgefahr. Die ständige Verfügbarkeit aller denkbaren Güter zu jeder Zeit
und zu günstigen Preisen stellt letztlich eine erhebliche Vereinfachung des alltäglichen Lebens dar.

Ankurbelung der weltweiten Wirtschaft

Insbesondere die Entwicklungs- und Schwellenländer profitieren in Form eines vergrößerten Arbeitsplatzangebotes von der
Globalisierung. Die Wirtschaftsleistung der einzelnen Staaten steigt, was langfristig auch zur Bekämpfung der Armut
beiträgt. Im Gegensatz zur so genannten Importsubstitution, die letztendlich darauf hinaus läuft, Importe mit hohen
Zöllen zu belegen, um so die inländische Wirtschaft zu fördern, haben erst die Globalisierung und der weltweite Handel
es Staaten wie Indien oder China ermöglicht, ihre Wirtschaft langfristig anzukurbeln. Die Globalisierung kann sich daher
also sowohl auf Unternehmen, als auch auf den einzelnen Menschen positiv auswirken: Die Chance für Unternehmen besteht
in der Ausdehnung auf neue Märkte und besseren Möglichkeiten, ihre Produkte zu verkaufen und Handel zu treiben. Vom
dadurch entstehenden gesellschaftlichen Wohlstand können auch die Bewohner des jeweiligen Staates Nutzen ziehen.

Mobilität von Gütern und Personen

Dank der weltweiten Vernetzung ist es heutzutage möglich, Güter innerhalb kurzer Zeiträume an Zielorte auf der ganzen
Welt zu transportieren. Das gleiche gilt für Personen, denn noch nie war es so einfach, ohne komplizierten
bürokratischen Aufwand ferne Länder zu bereisen. Dies trägt zusätzlich zur Ankurbelung des weltweiten Handels und des
Wachstums der Weltwirtschaft bei. Von der weltweiten Reisefreiheit profitiert insbesondere die Tourismusbranche.

Globale Zusammenarbeit und Angleichung der Kulturen

In einer globalisierten Welt sind nicht nur die Wirtschaft und der Handel weltweit vernetzt, sondern auch die Forschung
findet zunehmend auf globaler Ebene statt. Das verbessert die Bedingungen für die Forschung und erhöht die
Innovationskraft, da alle Länder von technischen Fortschritten deutlich schneller profitieren können. Dank der
Globalisierung des Wissens und der Forschung gilt das auch für Erkenntnisse und Fortschritte im Bereich der Medizin. Und
auch in Krisen und Konflikten erweist sich die globale Zusammenarbeit als vorteilhaft: Gerät ein Staat in
wirtschaftliche Not, kann er Hilfe von anderen Staaten erhalten, um eine Ausuferung und Ausbreitung der Krise zu
verhindern. Zudem kann die weltweite Vernetzung dazu beitragen, die Einhaltung der Menschenrechte weltweit besser
durchzusetzen und sie auf lange Sicht zum gesellschaftlichen Standard zu machen. Zur Angleichung der Kulturen tragen
auch neue Kommunikationstechnologien bei: Nicht nur Unternehmen sind global vernetzt, sondern auch Individuen können mit
Hilfe von Mobiltelefonen und über das Internet mit Menschen auf der ganzen Welt Kontakt aufnehmen. Dies führt nicht
zuletzt zu erheblichen Innovationen in der Nachrichten- und Informationstechnik, die sich zusätzlich positiv auf den
weltweiten Demokratisierungsprozess auswirken können.

<https://www.globalisierung-fakten.de/globalisierung-informationen/vorteile-und-nachteile-der-globalisation/>

## Nachteile

Probleme und Krisen erfahren durch die Globalisierung eine internationale Verflechtung. Sie können oftmals nicht mehr
rein national gelöst werden, sondern erfordern stattdessen ein hohes Maß an internationaler Kooperation.

Negative Folgen für den Arbeitsmarkt

Sowohl in den Industrieländern, als auch in den Entwicklungs-und Schwellenländern kann sich die globale Wirtschaftsweise
negativ auf den Arbeitsmarkt auswirken, wenn auch in ganz unterschiedlicher Weise. Arbeitsplätze, die für wenig
qualifizierte Arbeitnehmer geeignet sind, beispielsweise im Bereich der Warenproduktion, werden zunehmend in
Billiglohnländer verlagert. Darunter leiden nicht nur die Industriestaaten, sondern auch die Arbeitnehmer in diesen
Billiglohnländern, die nicht selten schlechte Arbeitsbedingungen wie unverhältnismäßig lange Arbeitszeiten und eine
ungenügende soziale Absicherung hinnehmen müssen. Auch Kinderarbeit ist in diesen Ländern noch immer aktuell.

Verschärfter Wettbewerb

Der verschärfte weltweite Wettbewerb lässt den Schwachen nur wenige Aufstiegschancen. Stattdessen werden die Starken
immer stärker, wodurch Wohlstand sehr ungerecht verteilt wird und die Schere zwischen armen und reichen Ländern immer
weiter auseinander klafft. Großkonzerne verdrängen die kleinen Unternehmen, was in besonderem Maße für die lokalen
Industrien gilt. Diese mächtigen Global Player haben zudem nicht selten großen Einfluss auf die Politik eines Landes,
was ebenfalls von vielen Seiten kritisiert wird. Die Gewinnmaximierung steht für diese Großkonzerne in der Regel im
Zentrum ihres Interesses, wofür viele Menschen Hungerlöhne und schlechte Arbeitsbedingungen in Kauf nehmen müssen.

Belastung der Umwelt

Kritiker betonen immer wieder die negativen Auswirkungen, die die Globalisierung auf die Umwelt hat. Insbesondere der
hohe Grad an Spezialisierung führt dazu, dass Waren nicht mehr komplett an einem Ort produziert und montiert werden,
sondern dass statt dessen Einzelteile auf der ganzen Welt gefertigt und dann rund um den Globus transportiert werden.
Hinzu kommen die wirtschaftlichen Fortschritte der Schwellenländer, die den Ausstoß von Kohlenstoffdioxid zusätzlich
erhöhen. Hinzu kommt ein durch die Globalisierung theoretisch unbegrenztes Warenangebot bei einer begrenzten Nachfrage.
Die Folge ist eine Überproduktion und somit die Verschwendung von wichtigen Ressourcen.

Verschärfung der globalen Kriminalität

Ein weiterer Nachteil der Globalisierung liegt in der Verschärfung der internationalen Kriminalität, die in den
letzten Jahrzehnten rasant angestiegen ist. Das internationale Strafrecht hat in diesem Bereich noch großen
Aufholbedarf, um zukünftig zu verhindern, dass Kriminelle in anderen Staaten untertauchen können, um einer Strafe zu
entgehen.

Dominanz der Industrienationen

Immer wieder wird kritisiert, dass in einer globalisierten Welt die Entwicklungsländer nur wenig Mitspracherecht haben,
wenn es um wichtige Entscheidungen geht. Diese werden von den Industrienationen ohne Einbeziehung der wirtschaftlich
schwächeren Staaten getroffen. Das verstärkt die Abhängigkeit der armen von den wohlhabenden Ländern. Zudem müssen
wirtschaftlich schwächere Länder ihre Preise niedrig halten, um konkurrenzfähig zu bleiben. Zusätzlichen Anlass für
Konflikte geben die unterschiedlichen Voraussetzungen in den einzelnen Staaten, die dafür sorgen, dass der
Globalisierungsprozess überall mit einer anderen Geschwindigkeit abläuft, sowie die weltweite Beschleunigung der
Finanzströme und die damit erhöhte Gefahr für Krisen und eine außer Kontrolle geratende Wirtschaft.

<https://www.globalisierung-fakten.de/globalisierung-informationen/vorteile-und-nachteile-der-globalisation/>
