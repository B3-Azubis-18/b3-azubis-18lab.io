---
layout: post
title:  "06. OSI-Schichtenmodell"
date:   2019-12-12 10:00:00 +0100
category: 2. Lehrjahr
tags: [Vernetzte Systeme]
---

## Kommunikationsprozesse im Schichten-Modell

Kommunikationsprozesse in Schichten zu organisieren und darzustellen, hat verschiedene Vorteile. Es reduziert die
Komplexität des Gesamtprozesses auf einzelne Teilfunktionen und erleichtert damit deren Nachvollziehbarkeit,
Weiterentwicklung und eine mögliche Fehlerbehandlung. Dies klingt vielleicht noch etwas abstrakt, soll aber am
folgenden Beispiel verdeutlicht werden:  
Zwei Diplomaten, einer in Paris und sein Amtskollege in Berlin, bereiten ein Gipfeltreffen vor. Zur Überbrückung von
möglichen Sprachproblemen werden auf beiden Seiten Dolmetscher eingesetzt. Telefone stellen die Übertragungstechnik dar.

## OSI vs. ISO

TODO: fix content

ISO, the International Organization for Standardization, ls tho world'r. largest developer of international standards
for n wldo vnrloty of producto and servlces. ISO ls not an acronym for thc orgnnlzotlon's nnmc; rothor the ISO term
ls based on the Grcek word "lsos", meonlng cqual. Tho In• ternatlonal Organlzatlon for Standardlzatlon chosc the ISO
term to offlrrn its position as being equal to all countries. IJnlUlfl. ISO OSI In networking, ISO is best known for
lts Open Systems lnterconnectlon (OSI) reforenco modol. ISO published the OSI reference model In 1984 to develop a
layered framework for networklng proto-cols. The original objective of this project was not only to create a
reference model but also to serve as a foundation for a suite of protocols to be used for the Internet. Thls was known
as the 051 protocol suite. However, due to the rlslng popularlty of the TCP/IP sulte, developed by Robert J Kahn,
Vinton Cerf and others, the 051 protocol sulte was not chosen as the protocol sulte for the Internet. Instead, the
TCP/IP protocol sulte was selected. The 051 protocol suite was implemented on telecommunications equlpment and can
still be found In legacy telecommunication networks. 

Initially the 051 model was designed by the ISO to provide a framework on whlch to bulld a sulte of open systems
protocols. The vision was that this set of protocols would be used to develop an international network that would not
be dependent on proprietary systems. 

Ultimately, the speed at which the TCP/IP-based Internet was adopted, and the rate at whlch lt expanded, caused the
development and acceptance of the OSI protocol sulte to lag behlnd. Alt-hough a few of the developed protocols using
the 0SI specifications are widely used today, the q seven-layer 051 model has made major contributions to the
development of other protocols and products for all types of new networks. The OSI model provides an extensive 1ist of
functions and services that can occur at each layer. lt also describes the interaction of each layer with the layers
directly above and below lt. (0: Cisco Networklns Ac11de111y, lntroductlon to Networks) 

## Das OSI-Sieben-Schichten-Modell

Jede der sieben Schichten hat eine besondere Aufgabe. Sie interagiert in der jeweils darüber un darunterliegenden
Schicht. In jedem Kommunikatonsprrozess werden immer alle Schichten vertikal durchlaufen; beim Senden von oben nach
unten, beim Empfang in umgekkehrter Reihenfolge. Bei der horizontalen Kommunikation ist der Informationsaustausch mit
dem Kommunikationspartner von Bedeutung. Beispiel: Aktionen des Protokolls TCP am Sender interagieren mit der
TCP-Funktion des Empfängers. Dies wird mitunter auch als virtuelle Kommunikation bezeichnet.    
Die Schichten 1 bis 4 gelten dabei als sogennante Transport- oder Netwerkorientierte Schichten und die Schichten 5 bis 7
als anwendungsorientiert.

![VNS - OSI-Modell](/assets/img/VNS/06-osi-modell.png)

## Das DoD-Schichtenmodell

Entwickelt von der DARPA für das Department of Defense (DoD), ging es um eine rein militärische Nutzung des ARPANETs,
welches durch eine dezentrale Struktur vor Ausfällen geschützt werden sollte. Das OSI-Schichtenmodell wurde einige
Jahre nach dem DoD-Modell entwickelt.
