---
layout: post
title:  "01. Netzwerktopologien"
date:   2019-02-17 06:00:00 +0100
category: 1. Lehrjahr
tags: [Vernetzte Systeme]
---

Persönliche Meinung der Redaktion: Der Wikipediaartikel ist definitiv lesenswert zum Thema
[Topologien](https://de.wikipedia.org/wiki/Topologie_(Rechnernetz)).

## Bus-Topologie

Die Bus-Topologie ist das einfachste Grundmuster für die Verkabelung.

Wenn ein Netzwerk oder ein Netzwerk-Segment der Bus-Topologie folgt, dann gibt es eine Hauptleitung (auch Backbone oder
Bus genannt), und die Kabel, mit denen die PCs angeschlossen werden, sind an die Hauptleitung (mindestens ein Adernpaar)
direkt angeschlossen. Die Stationen oder Geräte sind gegenüber dieser Leitung so positioniert, wie es meistens Häuser zu
einer Straße sind.

Die Hauptleitung (Backbone), welche die Geräte miteinander verbindet, kann z.B. mit verdrillten Leitungen, Koaxialkabel
oder flache Buskabeln realisiert werden.

In einem Bus-Netz überprüft jeder Computer jedes Paket, das über das Kabel versendet wird. Stellt ein Computer fest,
dass die Zieladresse des untersuchten Pakets mit der eigenen Adresse übereinstimmt, fertigt er eine Kopie an und
übernimmt diese. Es kann also immer nur ein Computer Nachrichten senden, alle anderen empfangen sie.

Damit die Signale davon abgehalten werden, am Ende der Leitung abzuprallen und als Echo reflektiert zu werden, hat jedes
Kabelende einen sogenannten Abschlusswiderstand. Dabei handelt es sich um ein Stück Hardware, welches das Signal
absorbiert, bevor es auf das Kabel zurückkehren kann.

Bus-Topologien sind einfach zu verwalten und erweitern, weil dafür weniger Kabel benötigt wird als bei anderen
Topologien. Da jedoch alle Computer an ein einziges Kabel angeschlossen sind, kann eine Unterbrechung im Kabel an jeder
möglichen Stelle das gesamte Netzwerk lahmlegen.

Da das Netzwerk zu jeder Zeit nur eine Nachricht über das Hauptkabel schicken kann, hängt die Netzwerkleistung stark
davon ab, wie viele Computer angeschlossen sind und in welchem Umfang die angeschlossenen PCs das Netzwerk nutzen. Durch
geregelte Zugriffsverfahren und die Begrenzung der Geräteanzahl kann man gewährleisten, dass die Netzwerklast nicht zu
groß wird und die Netzwerkleistung nicht zu stark abfällt.

### Schaubild

![01 - Netzwerktopologie_Bus](/assets/img/VNS/01 - Netzwerktopologie_Bus.png)

Quelle: [Wikipedia](https://upload.wikimedia.org/wikipedia/commons/3/32/Netzwerktopologie_Bus.png)

### Aktuelle Anwendungen

- USB (Universal Serial Bus):
  - USB 2.0: halbduplexfähig
  - USB 3.0: vollduplexfähig
- Bussysteme im industriellen Produktionsumfeld auf Feldbusebene
  - Realisierung von kompakten Produktionszellen
  - ASI Bus (Actuator-Sensor-Interface)
  - Profibus (Siemens)
- CAN-Bus
  - Standard für die Vernetzung in Automobilen

### Funktion des Abschlusswiderstandes

![Analogie Abschlusswiderstand](/assets/img/VNS/01 - Analogie Abschlusswiderstand.png)

Andere Analogien:

- Wellenbrecher
- Tonstudio
- Knautschzone beim Auto

### Vor- und Nachteile

| Vorteile                  | Nachteile                                       |
| ------------------------- | ----------------------------------------------- |
| man benötigt wenig Kabel  | es kann nur ein Computer senden                 |
| einfach erweiterbar       | Unterbrechung kann das ganze Netzwerk lahmlegen |
| kein Hub/Switch notwendig | je mehr PCs, desto weniger Leistung             |
| einfach zu verwalten      |                                                 |
| geringe Kosten            |                                                 |

## Ring-Topologie

Bei einem Netzwerk mit Ring-Topologie sind die Computer direkt oder über angeordnete Netzwerkkomponenten ringförmig
miteinander verbunden. Es gibt keine Kabelenden mit Abschlusswiderständen.

Die Daten durchlaufen den Ring (z.B. beim älteren Token-Ring-Verfahren) in einer Richtung und passieren dabei jeden
Computer. Im Gegensatz zu passiven Bus-Topologie, funktionieren die einzelnen Computer oder Netzwerkkomponenten wie
Repeater. Sie senden die Daten, die sie empfangen, verstärkt zum nächsten Computer weiter.

Da die Daten jeden Computer eines Computerringsystems durchlaufen, hat der Ausfall eines Computers Einfluss auf das
gesamte Netzwerk.

Ein großer Vorteil bei der Ringtopologie ist, dass ab einer bestimmten Anzahl von Teilnehmern der Verkabelungsaufwand
geringer als bei der Sterntopologie ist.

Moderne Netzwerkkomponenten im Produktionsumfeld können mit Leistungsunterbrechungen und defekten Gerätschaften
"umgehen". Fällt z.B. bei einem Ring mit 5 Knoten die Netzwerkkomponente 3 aus, so kann sich das physikalische
Ringsystem zu einer Linientopologie (2-1-5-4) rekonfigurieren.

<u>**Die Linientopologie ist nicht mit der Bustopologie zu verwechseln!**</u>

Um eine schnelle, automatische Wandlung bei Ausfall oder Unterbrechung vom physikalischen Ring zu Linie vornehmen zu
können, müssen folgende technische Voraussetzungen erfüllt sein:

- Die Kommunikation zwischen benachbarten Netzkomponenten muss vollduplexfähig sein, d.h. es können - anders als bei
  älteren Token-Ring-Systemen - Datenpakete in beide Richtungen gesendet werden.
- Eine Netzwerkkomponente (Manager) muss den Ring überwachen, Fehler erkennen und das Netzwerk sehr schnell
  rekonfigurieren, um die Produktion nicht zu beeinflussen.

Möglichkeit der Überwachung und Rekonfiguration von zeitkritischen Ring-Topologien:

Für die Überwachung der Funktion ist der Redundanzmanager zuständig. Er sendet Testpakete (Telegramme) in beide
Richtungen des Rings an die Redundanz-Clients, die sie weiterleiten.

Fällt nun eine Netzwerkkomponente aus oder wird eine Leitung unterbrochen, so kommen die gesendeten Testpakte nicht mehr
beim Redundanzmanger an. Nun rekonfiguriert der Redundanzmanager den physikalischen Ring innerhalb weniger
Zehntelsekunden neu und schließt die beiden Teile links und rechts von ihm zu einer neuen vollduplexfähigen Linie.

### Schaubild

![2018-12-20 - Netzwerktopologie_Bus](/assets/img/VNS/01 - Netzwerktopologie_Ring.png)

Quelle: [Wikipedia](https://upload.wikimedia.org/wikipedia/commons/7/71/Netzwerktopologie_Ring.png)

### Vor- und Nachteile

| Vorteile                                                     | Nachteile                                |
| ------------------------------------------------------------ | ---------------------------------------- |
| kürzere Vernetzungsentfernungen als z.B. bei Stern (> 6 PCs) | Ausfall eines Knotens legt Netzwerk lahm |
| bei Doppelring $$\rightarrow$$ Leistungssteigerung             | schwieriger erweiterbar                  |
| sichere, feste Übertragungsgeschwindigkeit                   |                                          |
| PCs sind Repeater                                            |                                          |

### Moderne Ringtopologien

z.B. im Produktionsumfeld

Bei Ausfall eines Segments:

- Redundanzmanager sendet Testpakete und erkennt Unterbrechung
- Rekonfiguration zur Linie

## Stern-Topologie

Stern-Topologien hat es zuerst in den 1960ern in Großrechner-Umgebungen gegeben. Im Zentrum gab es einen Großrechner, an
diesen war eine Reihe von Terminals angeschlossen. Jede Nachricht, die von einem Terminal zu einem anderen ging, nahm
ihren Weg über den Großrechner.

Wenn PCs in den 1980ern nach der Stern-Topologie verbunden wurden, gab es im Zentrum einen Sternverteiler (Hub) als
zentrale Verkabelungsstation. Wenn ein PC Daten sendete, wurden diese Daten über den Hub an jeden anderen Computer im
Netz übertragen.

Heutzutage kommen Switche für die sternförmige Verkabelung zum Einsatz. Sie können Ports schalten, also z.B. Port 1 mit
5 und parallel dazu Port 3 mit 4 "durchschalten". Hier in diesem Beispiel sind also zwei Verbindungen gleichzeitig
voll-duplex möglich. Die Schaltzeichen von Switches deuten dies auch an.

Nachteile einer Stern-Topologie: Jeder Computer muss mit dem zentralen Verteiler verbunden werden. Das erfordert unter
Umständen den Einsatz von einer großen Menge an Kabeln. Außerdem ist der Netzwerkbetrieb in hohem Maße von der
Funktionstüchtigkeit des zentralen Gerätes abhängig. Bei einem Ausfall der Zentrale kommt es zu einem Stillstand des
gesamten Netzes.

Andererseits gibt es keine Störanfälligkeit in Bezug auf den einzelnen Computer. Wenn in einem Stern-Netz ein Computer
ausfällt, dann werden die anderen dadurch nicht behindert, und wenn ein Kabel bricht, dann ist nur der Computer
betroffen, der über dieses Kabel an den Verteiler angeschlossen ist.

### Switches in der Produktionstechnik

Häufig werden Switches auch in Produktionen eingesetzt. Sie verbinden Anlagenteile, Produktionszellen, Roboterarme,
Motoren und Kameras sternförmig miteinander. Die Switches untereinander sind jedoch häufig physikalisch als Ringe oder
Linien miteinander verbunden.

### Schaubild

![2018-12-20 - Netzwerktopologie_Stern](/assets/img/VNS/01 - Netzwerktopologie_Stern.png)

Quelle: [Wikipedia](https://upload.wikimedia.org/wikipedia/commons/5/53/Netzwerktopologie_Stern.png)

### Vor- und Nachteile

| Vorteile                                                     | Nachteile                                             |
| ------------------------------------------------------------ | ----------------------------------------------------- |
| einzelne PCs können ausfallen $$\rightarrow$$ kein Netzausfall | jeder PC muss mit zentralem Hub/Switch verbunden sein |
| Kabel können brechen $$\rightarrow$$ kein Netzausfall        | Hub muss funktionieren, sonst kein Netzwerkbetrieb    |
| leicht erweiterbar                                           | großer Verkabelungsaufwand                            |
| leicht wartbar/leichte Fehlersuche                           |                                                       |
