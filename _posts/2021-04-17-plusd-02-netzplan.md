---
layout: post
title:  "02. Netzplan"
date:   2021-04-17 12:00:00 +0200
category: 2. Lehrjahr
tags: [PlusD]
---

Quelle des folgenden Artikels, gekürzt für die Berufsschule: <https://t2informatik.de/wissen-kompakt/netzplan/>

Ein Netzplan stellt Vorgänge, deren Dauer und zeitliche Anordnung sowie logische Abhängigkeiten grafisch oder
tabellarisch dar.

## Definition

Nach DIN 69 900, ist ein Netzplan eine „grafische oder tabellarische Darstellung einer Ablaufstruktur, die aus Vorgängen
bzw. Ereignissen und Anordnungsbeziehungen besteht“.

Ein Netzplan stellt folgende Dinge dar:

- einzelne Vorgänge,
- deren Dauer und
- zeitliche Anordnung sowie
- logische Abhängigkeiten dar.
  
Hierdurch wird die Berechnung von Anfangs- und Endzeitpunkten von Vorgängen sowie die Kalkulation von Pufferzeiten
ermöglicht.

Es gibt verschiedene Netzplantechniken, die zwei größten sind:

- PERT - Program Evaluation and Review Technique
- CPM - Critical Path Method

## Funktionen

- hilft bei der Ermittlung der Gesamtdauer eines Projekts.
- legt die zeitliche und logische Abfolge der Vorgänge in einem Projekt fest.
- visualisiert den kritischen Pfad und somit die Vorgänge, die das geplante Projektende gefährden können.
- stellt mögliche Puffer bzw. Reserven in der Terminplanung dar.

## Vorteile

Netzpläne eignen sich gut zur Projektplanung, denn

- sie helfen, Aktivitäten zu identifizieren, die zur Erfüllung von Aufgaben und somit zur Erreichung der Projektziele
  notwendig sind.
- sie fordern die Festlegung von logischen und zeitlichen Anordnungen der Vorgänge und helfen so, möglicherweise
  kritische Aspekte frühzeitig zu erkennen.
- sie visualisieren die Vorgänge, die auf dem kritischen Pfad liegen und wesentlich für die Erreichung der
  Terminvorgaben bzw. Projektziele sind.

Netzpläne eignen sich auch zur Projektsteuerung, denn

- frühzeitig erkannte Ressourcenengpässe auf dem kritischen Pfad können gegebenenfalls vermieden werden.
- Puffer lassen sich aktiv in die Terminplanung einbauen, so dass Mitarbeiter flexibel eingesetzt werden können.
- sie fördern das Verständnis der Beteiligten, die Kommunikation und die Kontrolle.
- sie ermöglichen die Prognose von Meilensteinen, der Projektdauer und des Projektendes.
- durch eine fortlaufende Aktualisierung sind sie ein kontinuierliches und nicht nur ein initiales Hilfsmittel im
  Projektmanagement.

## Elemente & Syntax

Ein Netzplan ist ein Mittel aus der **Graphentheorie**, das aus Knoten und Pfeilen besteht. Er kennt drei wesentliche
Elemente:

- **Vorgang**: ist eine Aktivität mit einem frühesten und spätesten Anfangs- und Endzeitpunkt.
- **Ereignis**: ist ein festgelegter, beschreibbarer Zustand im Projektablauf.
- **Anordnungsbeziehung**: ist die logische – also fachliche, technische und auch personelle – und die zeitliche
  Abhängigkeit zwischen einzelnen Vorgängen festgelegt; sie besteht immer zwischen genau zwei Knoten.

Der **Vorgangsknoten** wird im Netzplan meist als Rechteck und manchmal als Kreis dargestellt. Er hat einen
Vorgangsnamen und eine Vorgangsnummer bzw. einen Index. Weitere Informationen sind die Dauer der Aktivität, jeweils zwei
Zeitpunkte für den Start und das Ende der Aktivität, sowie zwei Werte für Puffer.

![BWP - Netzplan - 01](/assets/img/plusd/02-02-netplan-01.jpg)

## Der Puffer

Der Puffer einer Aktivität ist die Dauer, um den sich die Aktivität verzögern könnte, ohne sich auf die Dauer des
Gesamtprojekts auszuwirken. Ein Puffer ist damit eine Reserve bzw. ein Zeitpolster. Er entsteht durch parallel zu
bearbeitende Aktivitäten, die unterschiedliche Bearbeitungszeiten haben. Sofern eine Verschiebung kleiner als ein
definierter Puffer ist, hat dies keine Auswirkung auf das angestrebte Projektende.

Die Netzplantechnik kennt zwei verschiedene Puffer: Der Gesamtpuffer gibt an, um wie viel Tage eine Aktivität verschoben
werden kann, ohne den spätmöglichsten Anfangszeitpunkt der nachfolgenden Aktivität zu verschieben. Der Freie Puffer gibt
an, wie viel Tage eine Aktivität dauern kann, ohne die frühestmögliche Anfangszeit der nachfolgenden Aktivität zu
tangieren. Der Freie Puffer eines Vorgangs kann nie größer als der Gesamtpuffer des Vorgangs sein.

## Wie berechnet man die Dauer einer Aktivität?

In der Netzplantechnik PERT entsprechen die Pfeile zwischen zwei Knoten den Aktivitäten. Die Knoten selbst sind
Ereignisse, wobei das Anfangs- und Endereignis besonders hervorgehobene Arten von Ereignissen darstellen. Die Zeit zur
Durchführung einer Aktivität wird mithilfe der sogenannten Dreizeitschätzung ermittelt. PERT kennt also drei
verschiedene Zeitschätzungen: die minimale bzw. optimistische (`to`), die häufigste bzw. wahrscheinliche (`tw`) und die
maximale bzw. pessimistische (`tp`) Zeitschätzung. Aus diesen drei Werten lässt sich die erwartete Zeit (`te`) mit
folgender Formel – auch als Beta-Verteilung bezeichnet – berechnen:

$$
t_e = \frac{(t_o + 4 * t_w + t_p)}{6}
$$

Diese Herangehensweise führt auch zu einer veränderten Darstellung, bei der Zeitschätzungen an den Pfeilen und nicht in
oder an den Knoten dargestellt werden:

![BWP - Netzplan - 01](/assets/img/plusd/02-02-netzplan-02.jpg)

## Der kritische Pfad

Im Laufe eines Projekts wird es immer eine Reihe von Aktivitäten geben, deren Pufferzeit null ist. Puffer ergeben sich
durch parallele Aktivitäten, die beide Vorgänger einer bestimmten Aufgabe sind. Die kürzeste der parallelen Aktivitäten
hat die längste Pufferzeit, die längste der parallelen Aktivitäten hat keinen Puffer. Tritt bei dieser Aktivität eine
Verzögerung auf, verzögert sich das ganze Projekt. In der Netzplantechnik CPM werden diese Aktivitäten als kritische
Aktivitäten, kritische Aufgaben oder kritische Vorgänge bezeichnet. Der Pfad durch den alle kritischen Aktivitäten in
einem Projekt miteinander verbunden sind, ist der kritische Pfad. Er bestimmt die Mindestprojektdauer. Häufig wird
dieser kritische Pfad farblich – meist in rot – oder durch eine gestrichelte Linie hervorgehoben. Auch die Nennung der
Indexwerte in der Reihenfolge der Vorgänge ist üblich.

In Projekten kann es durchaus vorkommen, dass sich der kritische Pfad gabelt und so nebenläufige Wege nimmt; dabei hat
er aber immer nur eine einzige Anfangszeit und eine einzige Endzeit. Bei mehreren parallelen, kritischen Pfaden steigt
das Risiko, denn schon eine kleine Verzögerung einer Aktivität führt zur Verzögerung des gesamten Projekts. Dennoch ist
ein kritischer Pfad wichtig, denn durch ihn lassen sich einerseits alle zeitkritischen Aktivitäten identifizieren und
andererseits durch Zeitersparnisse die Gesamtprojektlaufzeit verkürzen. Grundsätzlich gibt es verschiedene Möglichkeiten
eine Gesamtprojektlaufzeit zu verkürzen:

- Aktivitäten parallel und nicht sequentiell durchführen
- Kritischen Aktivitäten zusätzliche Ressourcen zuordnen
- Kritische Aktivitäten durch Automatisierung beschleunigen

## Vorwärtsrechnung

Mit der Vorwärtsrechnung werden die frühestmögliche Anfangszeit und Endzeit der Aktivitäten kalkuliert. Dabei gelten
folgende Regeln:

- Bei der 1. Aktivität eines Netzplans ist die $$FAZ (\text{Früheste Anfangszeit}) = 0$$.
- Die FEZ (Früheste Endzeit) ergibt sich aus der Summe von $$FAZ + D (\text{Dauer})$$, also $$FEZ=FAZ+D$$.
- Hat eine Aktivität (Nachfolge-Aktivität bzw. Nachfolger) mehrere Vorgänger, so entspricht seine $$FAZ(n)$$ dem
  spätesten $$FEZ(v)$$ der Vorgänger.

Besondere Aufmerksamkeit muss bei der Vorwärtsrechnung auf die Anordnungsbeziehung zwischen Vorgängen gelegt werden:

- Ein Nachfolger kann beginnen sobald der Vorgänger fertig ist (Ende-Start- bzw. Ende-Anfang-Beziehung oder Normalfolge)
  gilt: $$FAZ(n) = FEZ(v) + \text{Verzögerung}(v)$$.
- Enden beide Aktivitäten gleichzeitig (Ende-Ende-Beziehung) gilt:
  $$FAZ(n) = FEZ(v) + \text{Verzögerung}(v) - \text{Dauer}(n)$$.
- Beginnen Vorgänger und Nachfolger gleichzeitig (Start-Start- bzw. Anfang-Anfang-Beziehung oder Anfangsfolge) gilt:
  $$FAZ(n) = FAZ(v) + \text{Verzögerung}(v)$$.

Die Puffer berechnen sich wie folgt:

- Gesamtpuffer: $$GP=SEZ-FEZ$$ oder $$GP=SAZ-FAZ$$.
- Feier Puffer: $$FP=FAZ(n)-FEZ(v)$$.

## Rückwärtsrechnung

Bei der Rückwärtsrechnung werden die spätmöglichste Anfangszeit und Endzeit der Aktivitäten kalkuliert. Dabei gelten
folgende Regeln:

- Bei der letzten Aktivität eines Netzplans ist FEZ (Früheste Endzeit) = SEZ (Späteste Endzeit).
- Die SAZ (Späteste Anfangszeit) ergibt sich aus $$SEZ – \text{Dauer}$$, also $$SAZ=SEZ-D$$.
- Hat eine Aktivität (Nachfolge-Aktivität bzw. Nachfolger) mehrere Vorgänger, so entspricht seine $$SAZ(n)$$ der
  spätesten Endzeit der Vorgänger $$SEZ(v)$$. Gibt es nur einen Vorgänger dann ist $$SAZ(n) = SEZ(v)$$.

Auch bei der Rückwärtsrechnung müssen die Anordnungsbeziehungen wie folgt berücksichtigt werden:

- Bei einer Ende-Start- bzw. Ende-Anfang-Beziehung oder Normalfolge gilt: $$SEZ(n) = SAZ(v) - \text{Verzögerung}(v)$$.
- Bei der Ende-Ende-Beziehung gilt: $$SEZ(n) = SEZ(v) - \text{Verzögerung}(n)$$.
- Bei einer Start-Start- bzw. Anfang-Anfang-Beziehung oder Anfangsfolge gilt:
  $$SEZ(n) = SAZ(v) - \text{Verzögerung}(v) + \text{Dauer}(n)$$.
- Ein Vorgang kann enden, sobald sein Vorgänger anfängt (bspw. kann eine alte Webseite erst abgeschaltet werden, wenn
  die neue Webseite läuft). Bei der sogenannten Sprungfolge oder Start-Ende- bzw. Anfang-Ende-Beziehung gilt:
  - $$SAZ(n) = FAZ(v) - \text{Verzögerung}(v) - \text{Dauer}(n)$$.
  - $$SEZ(n) = SEZ(v) + \text{Verzögerung}(v) + \text{Dauer}(n)$$.
