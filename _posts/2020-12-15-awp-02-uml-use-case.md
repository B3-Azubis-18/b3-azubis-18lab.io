---
layout: post
title: "02. UML - Use Case"
date: 2020-11-20 08:30:00 +0200
category: 3. Lehrjahr
tags: [Anwendungsprogrammierung]
---

Anwendungsfalldiagramme stellen eine zentrale Kommunikationsbasis zwischen Anforderungsermittler (Analytiker) und dem
Benutzer (Auftraggeber) dar. Besondere Bedeutung hat sie deshalb in der Analysephase eines objektorientierten
Entwicklungsprozesses, wo es darum geht festzustellen, was ein System leisten soll nicht wie das genau erfolgen soll.
Detaillierte Abläufe sind hier nicht erkennbar, sondern in anderen Verhaltensdiagrammen (z.B. Aktivitätsdiagramm,
Zustandsdiagramm).

Ein Anwendungsfall (Use Case) wird stets von einem Akteur angestoßen und beschreibt einen Geschäftsvorfall (bei der
Geschäftsprozess-Modellierung) oder eine typische Interaktion eines Anwenders (Akteur oder actor) mit einem
Computersystem (bei der Software-Modellierung).

## Graphische Darstellung

| Akteur | Anwendungsfall |
| ------ | -------------- |
| ![AWP - UML Use Case - Rolle des Akteurs](/assets/img/AWP/02-usecase-1.png) | ![AWP - UML Use Case - Anwednungsfall](/assets/img/AWP/02-usecase-1.png) |
| Die Rolle/Funktion die ein Mensch (bzw. ein externer Prozess, externes System) ausfüllt wird unter/in das graphische Symbol geschrieben. Das Strichmännchen steht eher für eine Person, im Rechteck steht eher ein externes System. | Der Name des Anwendungsfalls beschreibt die Funktionalität aus Sicht des Akteurs und sollte immer ein Verb beeinhalten |

Besteht zwischen einem Anwendungsfall und einem Akteur eine Beziehung, wird dies durch eine gerade Verbindungslinie
dargestellt. Die Anwendungsfälle eines Systems werden umrahmt und der Systemname angegeben.

![AWP - UML Use Case - Beziehung Akteur-System](/assets/img/AWP/02-usecase-3.png)

Beispiel: Anwendungsfalldiagramm Vermögensanlage
Ein Sachbearbeiter einer Unternehmung kauft Anlagen und überprüft die Liquidität. Nur der Finanzchef darf Risikoanlagen
kaufen.

![AWP - UML Use Case - Beziehung Beispiel](/assets/img/AWP/02-usecase-4.png)

## Beziehungen zwischen Anwendungsfällen

Neben den Verbindungen zwischen Akteuren und Anwendungsfällen lassen sich auch Beziehungen zwischen den
Anwendungsfällen darstellen.

### Die Enthält-Beziehung (include relationship)

Beispiel: Vermögensanlage
In dem Anwendungsfall „Anlage kaufen“ ist immer auch eine Liquiditätsprüfung enthalten.

![AWP - UML Use Case - Include Relationship Beispiel](/assets/img/AWP/02-usecase-5.png)

allgemeine graphische Darstellung der Enhält-Beziehung:

![AWP - UML Use Case - Include Relationship](/assets/img/AWP/02-usecase-6.png)

**Merke**:
Der Anwendungsfall A enthält den Anwendungsfall B. Anwendungsfall B wird immer verwendet, wenn Anwendungsfall A
auftritt. Der Pfeil ist vom benutzenden zum benutzten Anwendungsfall gerichtet.  
Die include-Beziehung eignet sich, etwas darzustellen, das in mehreren Anwendungsfällen vorkommt, um so Redundanzen zu
vermeiden.

### Die Erweitert-Beziehung (extend relationship)

Beispiel: Vermögensanlage
Wenn eine Anlage gekauft wird, muss ein Depot vorhanden sein. Wenn noch kein Depot vorhanden ist, muss eines angelegt
werden. Nimmt man den Schritt „Depot eröffnen“ hinzu, ergibt sich ein neuer Anwendungsfall. Dies ist eine Erweiterung
des ursprünglichen Verfahrens.

Abhängig davon, ob es ein Depot gibt, wird das „Depot eröffnen“ durchgeführt.
Man sagt: Der neue Anwendungsfall erweitert unter bestimmten Umständen den ursprünglichen Anwendungsfall
(Basis-Anwendungsfall).

![AWP - UML Use Case - Extend Relationship Beispiel](/assets/img/AWP/02-usecase-7.png)

allgemeine graphische Darstellung der Erweitert-Beziehung

![AWP - UML Use Case - Extend Relationship](/assets/img/AWP/02-usecase-8.png)

**Merke**:
Die extend-Beziehung von einem Anwendungsfall B zu einem Anwendungsfall A bedeutet, dass eine Instanz von Anwendungsfall
A unter bestimmten Bedingungen durch das im Anwendungsfall B beschreibende Verhalten erweitert werden kann.  
Anwendungsfall A kann alleine ausgeführt werden, die Funktionalität des Anwendungsfalls B ist eine optionale
Erweiterung.  
Im Anwendungsfall A sind extension points (ein oder mehrere Erweiterungspunkte) aufgelistet.  
Wenn nötig kann die Bedingung, die an einen extension point geknüpft ist, als Notiz mit dem Pfeil der
Erweiterungsbeziehung verbunden werden.

### Die Generalisierungs-Beziehung

Beispiel: Vermögensanlage
Der Kauf einer mit Risiko behafteten Anlage bedarf einer gesonderten Behandlung, die von dem Finanzchef persönlich
durchgeführt wird. Der Anwendungsfall "Risikoanlage kaufen" ist also somit ein Spezialfall des Anwendungsfalles "Anlage
kaufen".
Diese Spezialisierung stellt wie beim Klassendiagramm die Vererbungsbeziehung dar.

![AWP - UML Use Case - Generalisierungs-Beziehung](/assets/img/AWP/02-usecase-9.png)

## Vorgehensweise beim Erstellen von Use-Case-Diagrammen

1. Das System benennen und alle Akteure herausfinden.
2. Die wesentlichen Anwendungsfälle festhalten, ohne sich in Details zu verlieren (Beschreibung des Anwendungsfalls
   sollte ein Verb beinhalten. Er sollte einzeln ausgeführt werden können und muss mit einem Akteur verbunden sein).
3. Gemeinsame Anwendungsfälle ermitteln und mit ``<<include>>`` verbinden.
4. Anwendungsfälle notieren, die nur unter bestimmten Bedingungen andere Anwendungsfälle erweitern (``<<extend>>``).
5. Untersuchen ob Generalisierungs-Beziehungen zwischen Anwendungsfällen enthalten sein könnten.
