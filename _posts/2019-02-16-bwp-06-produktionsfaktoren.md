---
layout: post
title:  "06. Produktionsfaktoren"
date:   2019-02-15 06:25:00 +0100
category: 1. Lehrjahr
tags: [Betriebswirtschaftliche Prozesse]
---

- **Produktionsfaktoren**: sind sämtliche an der Produktion von Gütern bzw. dem Angebot von Dienstleistungen beteiligte
  Mittel.
- Ein Unternehmen kann nur erfolgreich und gewinnmaximierend produzieren, wenn die Produktionsfaktoren sinnvoll und
  zielgerichtet kombiniert wurden. Die Kombination der Ressourcen ist geschäftsabhängig.

![BWP - Produktionsfaktoren](/assets/img/BWP/06 - Produktionsfaktoren.png)
