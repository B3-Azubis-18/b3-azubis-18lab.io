---
layout: post
title:  "05. Wirtschaftskreislauf"
date:   2019-02-15 06:20:00 +0100
category: 1. Lehrjahr
tags: [Betriebswirtschaftliche Prozesse]
--- 

- Warum Kreislauf? - Es wird eine geschlossene Wirtschaft betrachtet. Die Summe der eingehenden und ausgehenden
  Güterströme ist null. Ausgaben = Einnahmen
- Der Geldkreislauf und der Güterkreislauf sind entgegengesetzt.

## Einfacher Wirtschaftskreislauf

![BWP - Einfacher Wirtschaftskreislauf](/assets/img/BWP/05 - Einfacher Wirtschaftskreislauf.png)

- **Faktorleistungen**: Haushalte überlassen die Produktionsmittel Kapital & Boden zur Nutzung einem Unternehmen.
- **Geldstrom Einkommen**: Gegenleistung der Unternehmen bspw. Löhne, Gehälter, Mieten, Pachten, Zinsen, Dividenden.
- **Güterstrom Konsumgüter**: Haushalte beziehen von Unternehmen Güter und Dienstleistungen.
- **Geldstrom Konsumausgaben**: Gegenleistung für Konsumgüter.

## Erweiterter Wirtschaftskreislauf

![BWP - Erweiterter Wirtschaftskreislauf](/assets/img/BWP/05 - Erweiterter Wirschaftskreislauf.png)
