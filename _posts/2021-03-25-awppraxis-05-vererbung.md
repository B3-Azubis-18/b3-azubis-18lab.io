---
layout: post
title: "05. Vererbung"
date: 2021-01-20 08:30:00 +0200
category: 3. Lehrjahr
tags: [Anwendungsprogrammierung, Praxis]
---

## Allgemeine Vererbung

Bei der Erstellung eines Programms zur Verwaltung der Kfz einer Firma müssen verschiedene Fahrzeugtypen berücksichtigt
werden:

- Omnibusse:
    - Eigenschaften: ..., Anzahl Sitzplätze, Anzahl Stehplätze, Tankinhalt, Verbrauch, ...
    - Benötigte Infos/Funktionen: ..., ermittle Reichweite, ermittle Anzahl aller Mitfahrer, ...
- PKW:
    - Eigenschaften: ..., Anzahl Sitzplätze, Tankinhalt, Verbrauch, Kofferraum-Volumen...
    - Benötigte Infos/Funktionen: ..., ermittle Reichweite, ermittle Anzahl aller Mitfahrer, ...
- LKW:
    - Eigenschaften: ..., Anzahl Sitzplätze, Tankinhalt, Verbrauch, max.Zuladung, ...
    - Benötigte Infos/Funktionen: ..., ermittle Reichweite, ermittle Anzahl aller Mitfahrer, ermittle Zuladung, ...

(Hinweis: Auszug aus möglichen Eigenschaften und Funktionalitäten)

### Klassendiagramm

Dafür werden entsprechende Klassen gebildet. In UML-Notation sieht dies folgendermaßen aus:

![AWP - Vererbung 01](/assets/img/AWP/05-vererbung-01.png)

### Neue Klassenstruktur

![AWP - Vererbung 02](/assets/img/AWP/05-vererbung-02.png)

### Syntax

![AWP - Vererbung 03](/assets/img/AWP/05-vererbung-03.png)

### Beispiele

![AWP - Vererbung 04](/assets/img/AWP/05-vererbung-04.png)

![AWP - Vererbung 05](/assets/img/AWP/05-vererbung-05.png)

![AWP - Vererbung 06](/assets/img/AWP/05-vererbung-06.png)

### Konstruktor & Destruktor

- Abgeleitete Klassen können sowohl Konstruktoren, als auch einen Destruktor haben.
- Der Konstruktor der Oberklasse wird immer vor dem Konstruktor der abgeleiteten Klasse abgearbeitet.
- Der Destruktor der Oberklasse wird immer nach dem Destruktor der abgeleiteten Klasse abgearbeitet.

### Parameter für Konstruktoren

Aufruf des Konstruktors der Oberklasse in der Initialisierungsliste mit einem Parameter aus der Parameterliste des
Konstruktors der abgeleiteten Klasse!

```cs
class CKFZ
{
    private stringMarke;

    public string getTyp() {return "Kfz";}
    public string getMarke() {return Marke;}
    public CKFZ(string myMarke) {Marke=myMarke;}
}

class CPKW : CKFZ
{
    public CPKW(string Marke) : base (Marke) {}
}


//Testanwendung:
void Test()
{
    CPKW pkw = new CPKW("Merday");
    Console.WriteLine (pkw.getTyp());
    Console.WriteLine (pkw.getMarke());
}
```

## Einfachvererbung & Mehrfachvererbung

![AWP - Vererbung 07](/assets/img/AWP/05-vererbung-07.png)

Unterklassen können nicht nur von einer, sondern auch von mehreren Oberklassen erben. Diesen Zusammenhang bezeichnet man
als Mehrfachvererbung.

![AWP - Vererbung 08](/assets/img/AWP/05-vererbung-08.png)

### Klassenarten

- Klassen, von denen keine Instanzen gebildet werden, nennt man abstrakte Klassen.
- Klassen, von denen Objekte erzeugt werden, bezeichnet man als konkrete Klassen.

![AWP - Vererbung 09](/assets/img/AWP/05-vererbung-09.png)

Abstrakte Klasse:  
Wird in C# realisiert, indem die Klasse selbst als abstrakt gekennzeichnet wird: `abstract class CMitarbeiter { ... }`

In C# können abstrakte Klassen auch abstrakte Methoden beinhalten.

Konkrete Klasse:  
Damit aus CAngestellter Objekte erzeugt werden können, müssen alle abstrakten Methoden der Oberklasse überschrieben
werden!

### Klassendiagramm

In Klassendiagrammen können unterschiedliche Beziehungsarten auftreten:

![AWP - Vererbung 10](/assets/img/AWP/05-vererbung-10.png)

- Komposition zwischen Rechnung und PositionenListe.
- Assoziation zwischen Kunde und Rechnung.
- Vererbungsbeziehung zwischen Person und Kunde.
