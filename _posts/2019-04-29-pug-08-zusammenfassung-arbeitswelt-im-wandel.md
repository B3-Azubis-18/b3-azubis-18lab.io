---
layout: post
title:  "08. Zusammenfassung Arbeitswelt im Wandel"
date:   2019-04-29 11:00:00 +0100
category: 1. Lehrjahr
tags: [Politik und Gesellschaft]
---

## Technologiefortschritt
- Automatisierung: weniger Arbeitsplätze, mehr Roboter
- mehr Dienstleistungsarbeitsplätze
- hohe Investitions-, geringere Lohnkosten
- hoher Fortbildungsbedarf bei Arbeitnehmern
- häufigere Anpassung an Veränderungen in der Arbeitswelt

## Globalisierung
- Produktionsstandort dort, wo die Kosten (z.B. für Löhne, Steuern, Energie, soziale Schutzrechte, Umweltauflagen) am niedrigsten sind
- Erschließung von neuen Märkten
- Produktion und Vertrieb der Produkte direkt im Ausland

## Flexible und mobile Arbeitnehmer
- Flexibilität: Arbeitnehmer passt sich durch hohe Selbstkompetenz und Bereitschaft zur Fortbildung an betriebliche Veränderungen an
- Mobilität: Arbeitnehmer ist aus beruflichen Gründen zum Wohnortwechsel oder zum Pendeln bereit
- Veränderungen sind Zukunftschancen

## Flexibilisierung der Arbeitszeit
- Arbeitszeitkonten: Verrechnung der Plus- und Minusstunden
- Jobsharing: zwei teilen sich einen Arbeitsplatz
- Abrufarbeit: Arbeitnehmer hält sich in häuslicher Umgebung arbeitsbereit
- Telearbeit: gearbeitet wird zu Hause
- Vertrauensarbeit: freie Zeiteinteilung

## Arten, Ursachen der Arbeitslosigkeit
- konjunkturelle AL: Wirtschaftsrückgang
- strukturelle AL: Wettbewerbsverlust
- saisonale AL: Jahreszeiteneinfluss
- friktionelle AL: Arbeitsplatzwechsel
- Mismatch-AL: Angebot entspricht nicht der Nachfrage nach Arbeitskräften

## Abbau von Arbeitslosigkeit
- Führen eines Jahresarbeitszeitkontos
- Weiterqualifizierung und Umschulungsmaßnahmen
- Überstunden durch Freizeit ausgeglichen
- bei Auftragsspitzen Leiharbeitskräfte einsetzen

## Schlüsselqualifikationen
- Fachkompetenz: spezielle Berufskenntnisse
- Handlungskompetenz: Fähigkeit des problemlösenden Denkens
- Sozialkompetenz: Kontaktfähigkeit, Teamfähigkeit, Toleranz usw.
- Selbstkompetenz: Kritikfähigkeit, Fähigkeit zur beruflichen, privaten Lebensgestaltung

## Berufsbegleitende Fortbildung
- inner- und außerbetriebliche Seminare
- Kurse an der Volkshochschule
- Seminare bei Handwerksinnungen, Berufsverbänden
- Selbststudium von Fachliteratur
- Fernlehrgänge

## Berufliches Schulwesen
- Technisches Gymnasium
- Berufliche Oberschule
- Technikerschule (z.B. für Elektrotechnik)
- Berufsfachschule (z.B. für Maschinenbau)
- Fachakademie (z.B. für Heilpädagogik)
- Fernstudium

## Berufliche Aufstiegsausbildung
- Meisterkurs (Führen eines Handwerkbetriebs, Ausbilden von Azubis)
- Technikerschule
- Kurse für Industriemeister, Bilanzbuchhalter, Industriefachwirte
- Fernstudium
