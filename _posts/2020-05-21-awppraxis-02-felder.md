---
layout: post
title: "02. Felder"
date: 2019-10-18 10:05:00 +0200
category: 2. Lehrjahr
tags: [Anwendungsprogrammierung, Praxis]
---

## Allgemeines

Ein Feld wird auch Array genannt. Streng genommen sind diese keine Funktion die C++ ist, sondern C. Intern ist ein Array
lediglich eine Sequenz von Objekten desselben Typs direkt hintereinander im Speicher allokiert.

## Syntax

Einen Array initialisieren:

````c
// Initialisierung eines Arrays
char chArray[10];

// Zugriff auf Elemente (lesend)
char *pch = chArray;        // Evaluates to a pointer to the first element.
char   ch = chArray[0];     // Evaluates to the value of the first element.
ch = chArray[3];            // Evaluates to the value of the fourth element.

// Zugriff auf Elemente (schreibend)
chArray[5] = 'a';
````

## Mehrdimensionale Felder

Da ein Feld lediglich eine Sequenz gleicher Objekte ist, kann ein Feld somit auch selbst als Elemente Felder haben.
Diese Verschachtelung ist theoretisch endlos möglich. Im folgenden ein paar Beispiele zur Erläuterung:

````c
// Initialisierung einiger Arrays
int i1[5][7];
int i2[10][2][5];

// Zugriff auf Elemente (lesend)
// Bsp. von: https://docs.microsoft.com/de-de/cpp/cpp/arrays-cpp?view=vs-2019#multidimensional-arrays
// using_arrays_2.cpp
// compile with: /EHsc /W1
#include <iostream>
using namespace std;
int main() {
   double multi[4][4][3];   // Declare the array.
   double (*p2multi)[3];
   double (*p1multi);

   cout << multi[3][2][2] << "\n";   // C4700 Use three subscripts.
   p2multi = multi[3];               // Make p2multi point to
                                     // fourth "plane" of multi.
   p1multi = multi[3][2];            // Make p1multi point to
                                     // fourth plane, third row
                                     // of multi.
}

// Zugriff auf Elemente (schreibend)
multi[2][3] = 5.3;
````

## Weiterführendes

In C++ wird nicht empfohlen diese C-Style Arrays zu benutzen. Stattdessen verwendet lieber `std::vector` oder
`std::array`.

Weiterführende Links und Quellen für diese Seite:

- [MS-Docs](https://docs.microsoft.com/de-de/cpp/cpp/arrays-cpp?view=vs-2019)
- [Offizielle C++ Referenz](http://www.cplusplus.com/doc/tutorial/arrays/)
- [W3 Schools](https://www.w3schools.com/cpp/cpp_arrays.asp)
