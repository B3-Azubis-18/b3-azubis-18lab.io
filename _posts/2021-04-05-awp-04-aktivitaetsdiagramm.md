---
layout: post
title: "04. Aktivitätsdiagramm"
date: 2021-01-15 08:30:00 +0200
category: 3. Lehrjahr
tags: [Anwendungsprogrammierung]
---

Aktivitätsdiagramme bieten die Möglichkeit, Prozesse abzubilden, die sich aus mehreren Aktionen zusammensetzen.

Eine Aktion ist ein elementarer Vorgang, der nicht weiter zerlegt werden kann oder soll. Eine Aktivität ist eine Folge
von Aktionen, die ein bestimmtes Ergebnis liefern und unter einem gemeinsamen Namen zusammengefasst werden. In dieser
Hinsicht ähneln Aktivitätsdiagramme den Programmablaufplänen, die in der EDV zur Darstellung von Programmabläufen
dienen.

Die wesentlichen Elemente eines Aktivitätsdiagramms sind

1. Aktionen
2. Steuerungselemente
3. Objektknoten
4. Verbindungskanten

Als Symbol für eine Aktion verwendet die UML ein Rechteck mit abgerundeten Ecken:

{% mermaid %}
graph TD
  id1(Aktionsname)
{% endmermaid %}

Abläufe ergeben sich durch die Verknüpfung mehrerer Aktionen durch so genannte Kanten. Dabei handelt es sich um
Verbindungspfeile (gerichtete Kanten), die eine Reihenfolge der durchzuführenden Aktionen festlegen.

Die Ablaufsteuerung erfolgt über Bedingungen, die beispielsweise zur Verzweigung oder Zusammenführung von Teilabläufen
führen. Damit sind auch parallele Prozesse in Aktivitätsdiagrammen darstellbar.

Beispiel für Verzweigung:

{% mermaid %}
graph TD
    id1(Aktion 1)
    id2{ }
    id3(Aktion 1.1)
    id4(Aktion 1.2)
    id1 --> id2
    id2 -- Bedingung1.1 --> id3
    id2 -- Bedingung1.2 --> id4
{% endmermaid %}

Objektknoten repräsentieren Daten oder Akteure, die im Rahmen einer Aktivität von Bedeutung sind. Sie werden durch ein
Rechteck repräsentiert, das mit dem Namen des Objektes beschriftet ist.

Beispiel:

{% mermaid %}
graph LR
  id1[Auftrag]
  id2(Auftrag abwickeln)
  id3[Rechnung]
  id1 -.-> id2
  id2 -.-> id3
{% endmermaid %}

## Aktionen

Durch die Verknüpfung von Aktionen über gerichtete Kanten (Pfeile) entsteht eine zeitlich geordnete Folge von Aktionen:

{% mermaid %}
graph LR
  id1(Aktion 1) --> id2(Aktion 2)
{% endmermaid %}

Wenn man eine Folge von Aktionen mit einem Rahmen versieht (einschließlich Beschriftung), werden Aktionen zu einer
Aktivität zusammengefasst:

{% mermaid %}
graph TD
    id1(Wasser in Kaffeemaschine füllen)
    id2(Filterpapier einlegen)
    id3(Kaffee in Filter einfüllen)
    id4(Kaffeemaschine einschalten)
    id1 --> id2 --> id3 --> id4
{% endmermaid %}

Der Text innerhalb des Aktionssymbols ist beliebig; es ist somit auch möglich, hier eine Ablaufbeschreibung z.B. mit
Pseudocode einzufügen. Wenn diese Ablaufbeschreibung allerdings zu umfangreich wird, sollte die Aktion besser als eigene
Aktivität modelliert werden.

## Steuerungselemente

Steuerungselemente regeln die zeitliche und logische Abfolge von Aktionen im Rahmen einer Aktivität. Neben Start und
Ende bilden sie Verzweigungen und Verbindungen von Abläufen und lenken parallele Vorgänge.

### Startknoten

Der Startknoten markiert den Beginn eines Ablaufs in einem Aktivitätsdiagramm. Von einem Startknoten dürfen beliebig
viele Kanten ausgehen, jedoch keine zu ihm hinführen!

Wenn in einem Aktivitätsdiagramm mehrere Startknoten vorkommen, so kann der Beginn alternativ oder auch parallel
erfolgen. Der Eintrittspunkt in ein Aktivitätsdiagramm kann auch ein Objektknoten sein.

### Endknoten

Ein Endknoten markiert das Ende eines Ablaufs. Dabei wird zwischen dem Ende eines Teilablaufs und dem Ende der gesamten
Aktivität unterschieden. Der Endknoten für Aktivitäten: `TODO: GRAFIK`, der für Teilabläufe: `TODO: GRAFIK`
Wenn nur ein Teilablauf beendet ist, können andere (parallele) Abläufe der Aktivität weiter laufen.

### Splitting und Synchronisation

Die Abläufe in einem Aktivitätsdiagramm können geteilt und wieder zusammen geführt werden. Durch die Teilung (Splitting)
entstehen parallele Abläufe, die zu zeitgleichen Aktivitäten führen. Umgekehrt werden parallele Abläufe durch
Synchronisation wieder zu einer gemeinsamen Folge von Aktionen zusammen geführt.

| Splitting | Synchronisation |
| --- | --- |
| ![AWP - Aktivitätsdiagramm - Splitting](/assets/img/AWP/04-aktivitaetsdiagramm-01.png) | ![AWP - Aktivitätsdiagramm - Synchronisation](/assets/img/AWP/04-aktivitaetsdiagramm-02.png) |
| Nach Ausführung von Aktion 1 leiten Aktion 2 und Aktion 3 zeitgleiche, voneinander unabhängige Abläufe ein. Der Balken bewirkt die Auftrennung. | Die zunächst parallel verlaufenden Aktionen A und B werden durch die Synchronisation am Balken zusammen geführt. Anschließend wird Aktion C ausgeführt. Bevor Aktion C ausgeführt werden kann, müssen beide Vorgänger (Aktion A und Aktion B) beendet sein! |

Das Beispiel "Frühstück vorbereiten" soll Splitting und Synchronisation veranschaulichen:

![AWP - Aktivitätsdiagramm - Frühstück Beispiel](/assets/img/AWP/04-aktivitaetsdiagramm-03.png)

### Verzweigung

Verzweigungen dienen in Aktivitätsdiagrammen dazu, alternative Abläufe darzustellen.

{% mermaid %}
graph TD
    id1(Deckung prüfen)
    id2{ }
    id3(auszahlen)
    id4(Zahlung verweigern)
    id1 --> id2
    id2 -- deckung=true --> id3
    id2 -- deckung=false --> id4
{% endmermaid %}

### Verbindung

Ein Verbindungsknoten führt mehrere Kanten zu einem einzigen Knoten zusammen:

{% mermaid %}
graph TD
    id1(Aktion C)
    id2{ }
    id3(Aktion A)
    id4(Aktion B)
    id3 --> id2
    id4 --> id2
    id2 --> id1
{% endmermaid %}

Im Gegensatz zur Synchronisation kann Aktion C starten, wenn zumindest eine der beiden Aktionen A oder B beendet sind.
Bei der Synchronisation müssen Aktion A und Aktion B zwingend beide beendet sein, bevor Aktion C startet.

## Objektknoten

Mithilfe von Objektknoten werden Daten und Werte in eine Aktivität eingebracht. Die Darstellung erfolgt über ein
Rechteck, das mit einer Beschriftung zur Kennzeichnung des Knotentyps versehen wird:

![AWP - Aktivitätsdiagramm - Objektknoten](/assets/img/AWP/04-aktivitaetsdiagramm-04.png)

Objektknoten können als Eingangs einer Aktion auftreten. Sie liefern dann Daten oder Werte, die erforderlich sind, um
die betreffende Aktion auszuführen:

![AWP - Aktivitätsdiagramm - Objektknoten](/assets/img/AWP/04-aktivitaetsdiagramm-05.png)

Für die Durchführung der Aktion "Kaffee zubereiten" wird ein Kaffeefilter-Objekt benötigt.

Ebenso können Aktionen Ergebnisse in Form von Daten oder Werten haben. Dann ergibt sich aus einer Aktion ein
Objektknoten:

![AWP - Aktivitätsdiagramm - Objektknoten](/assets/img/AWP/04-aktivitaetsdiagramm-06.png)

Für einen Objektknoten kann auch ein bestimmter Zustand angegeben werden, der in eckigen Klammern hinter dem Knotentyp
angegeben wird:

![AWP - Aktivitätsdiagramm - Objektknoten](/assets/img/AWP/04-aktivitaetsdiagramm-07.png)

## Kontrollfluss/Objektfluss

### Kontrollfluss

Als Kontrollfluss bezeichnet man eine Kante (Pfeillinie), die entweder 2 Aktionen oder eine Aktion mit einem
Steuerungselement verbindet:

![AWP - Aktivitätsdiagramm - Kontrollfluss](/assets/img/AWP/04-aktivitaetsdiagramm-08.png)

### Objektfluss

Ein Objektfluss ist eine Kante, die mindestens an einem Ende mit einem Objektknoten verbunden ist:

![AWP - Aktivitätsdiagramm - Objektfluss](/assets/img/AWP/04-aktivitaetsdiagramm-09.png)

Hier sind alle Kanten Objektflüsse (trotz des Steuerelementes zwischen Aktion 1 und Aktion 2 bzw. Aktion 3 erfolgt die
Verbindung der Aktionen über einen Objektknoten).

### Bedingungen

Kanten können an Bedingungen geknüpft werden. Ein Übergang von einer Aktion zur nächsten ist nur dann möglich, wenn die
Bedingung erfüllt ist. Die Bedingung wird in eckigen Klammern notiert:

![AWP - Aktivitätsdiagramm - Bedingungen](/assets/img/AWP/04-aktivitaetsdiagramm-10.png)

Übersichtlicher ist es, Bedingungen über Verzweigungen zu realisieren. Dadurch kann es auch nicht zu dem Fall kommen,
dass eine Aktivität aufgrund einer niemals wahr werden - den Bedingung irgendwo "hängen bleibt" und nicht ordnungsgemäß
beendet wird.

## Verantwortlichkeitsbereiche

Aktivitätsdiagramme können in Verantwortlichkeitsbereiche (swimlanes) aufgeteilt werden. Die einzelnen
Verantwortlichkeitsbereiche werden durch senkrechte Linien optisch voneinander getrennt. Jeder
Verantwortlichkeitsbereich stellt dabei eine Aktivität dar, die wiederum aus einzelnen Aktionen bestehen können. Bei der
Aufteilung des Aktivitätsdiagramms in Verantwortlichkeitsbereiche lassen sich auch die Aktivitäten unterschiedlicher
Objekte in einem einzigen Diagramm darstellen:

![AWP - Aktivitätsdiagramm - Verantwortlichkeitsbegreiche](/assets/img/AWP/04-aktivitaetsdiagramm-11.png)

Das Aktivitätsdiagramm zeigt das Verhalten der beiden beteiligten Objekte `konto1` und `konto2`.
