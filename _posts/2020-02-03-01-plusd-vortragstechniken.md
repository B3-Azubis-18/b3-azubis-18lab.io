---
layout: post
title:  "01. Vortragstechniken"
date:   2020-02-03 19:00:00 +0100
category: 2. Lehrjahr
tags: [PlusD]
---

## 1. Zielgruppenanalyse

Grundsatz:
Jedes Publikum benötigt eine individuelle Ansprachen, somit sollte ein Vortrag die spezifischen Erwartungshaltungen und
Interessen seines Publikums kennen bzw. herausfinden. Wenn man informieren will Spricht man somit mehr Zuhörer direkt
an.

Weitere Beispiele aus Übungen<br>
Finanziell potent, Dinkis (= double income, no kids), Idealisten, Technik-Interessierte, Stadtbewohner, Gewerbebetreiber, Immobilienbesitzer

| Was Zuhörer schätzen                                     | Was Zuhörer ablehnen                                           |
|----------------------------------------------------------|----------------------------------------------------------------|
| bedürfnisorientierte Präsentationen                      | fehlender Nutzen; kein Bezug zu deren Bedürfnissen             |
| gute Sicht und Hörverhältnisse                           | kleine Schriften; unverständliche Stimmen                      |
| einfache, leicht verständliches Informationsmaterial     | komplizierte und mit Details überladene Informationslawinen    |
| neue, interessante Tatsachen und Gedanken                | aufgewärmte und unerhebliche Informationen                     |
| mit Fakten untermauerte Aussagen                         | unbewiesene, als Tatsache hingestellte Meinungen, Behauptungen |
| notwendige Fachausdrücke mit Erklärung                   | Fachchinesisch und unverständliche Abkürzungen                 |
| knappe, präzise Informationen                            | langatmige, vage und nichtssagende Ausführungen                |
| übersichtliche Strukturen und Inhaltsangaben             | fehlende Gliederungen; rätselhafte Zusammenhänge               |
| Zusammenfassungen und Wiedereinstiegshilfen              | ungegliederter Informationsfluss mit abruptem Ende             |
| klare Entscheidungsgrundlagen Vorschläge und Anweisungen | schwammige Aussagen und unausgegorene Ideen                    |
| emotionale Anregungen und Inspirationen                  | Langeweile und Peinlichkeiten                                  |
| ehrliche Wertschätzung                                   | Schmeichelei und Arroganz                                      |
| Den Dialog mit den Zuhörern (Anliegen wahrnehmen)        | <br>einen Monolog über die eigenen Interessen führen           |

## 2. Verschiedene Einstiege

Einstieg: Der Einstieg ist dafür gedacht das Interesse zu wecken, dabei sollte man sich selbst und das Thema Vorstellen,
das Publikum motivieren, und die eigene Kompetenz untermauern.

Zehn Ideen (Von Hannes)

| Zitate und Aphorismen           | Wie Cesar einst sagte: "veni vidi vici" $$\rightarrow$$ "Ich kam, ich sah, siegte"     |
|---------------------------------|----------------------------------------------------------------------------------------|
| Aktualität aufzeigen            | einen Zusammenhang zu tagesaktuellen Beispielen aufbauen                               |
| eine Frage stellen              | eine Frage zu stellen die zum Mitdenken einlädt                                        |
| das Feld von hinten aufrollen   | Fazit von ende an Anfang ziehen                                                        |
| mit einer Geschichte einsteigen | Geschichte erzählen                                                                    |
| die Versprechung                | "Ich werde für 15 min sprechen und am Ende wird jeder in der Lage sein XY zu erklären" |
| ein aktivierender Einstieg      | Fragen stellen und Publikum darüber abstimmen Lassen                                   |
| spannende Fakten                | Selbsterklärend                                                                        |
| die Provokation                 | provozierende Frage zum Einstieg stellen gepaart mit einer kleinen Geschichte          |
| mit einem Gegenstand starten    | physikalisches Objekt Thema: du Objekt Dicke Dinger                                    |

## 3. Gestaltung von Folien und deren Zweck

| Kriterium          | Beispiele                                                                                                    |
|--------------------|--------------------------------------------------------------------------------------------------------------|
| demografisch       | - Alter<br>- Geschlecht<br>- Familienstand                                                                   |
| geografisch        | - Wohnort<br>- Herkunft                                                                                      |
| sozioökonomisch    | - Haushaltsgröße<br>- Einkommen<br>- Kaufkraft<br>- Bildung & Ausbildung<br>- Beruflicher Status<br>- Besitz |
| psychografisch     | - Interessen<br>- Motive<br>- Lebenseinstellung                                                              |
| Verhaltensmerkmale | - Kaufgewohnheiten<br>- Kommunikationsverhalten<br>- Mediennutzung                                           |

Fazit: Versuch die Aufmerksamkeit deiner Präsentation immer auf dir und nicht auf der PowerPoint zu Halten oder Falls
nötig nur kurz um etwas zu erläutern

## 4. Einen Vortrag Gliedern

Einleitung:
Es ist wichtig die Teilbereiche der Präsentation anhand des Themas zu Gliedern
Bsp. E-Mobilität
1. einleitende Geschichte
2. Missstände aktueller Mobilität
3. Lösungen für 2 Anbieten
4. Resümee mit wichtigsten Fakten
Dabei sollte die Einleitende Geschichte etwa 10% des Gesamtvortrags ausmachen.
Zeige dabei auch auf was die Zuhörer in dem Vortrag erwartet

Der Hauptteil würde in unsrem Beispiel 2. Und 3. Enthalten (etwa 80% der Zeit)
Hierbei würde man seine Fakten vorbringen und je nach Zielgruppe Erklären und bestärken.
Faustregel: Soviel Stoff wie nötig , auch den Hauptteil für sich selber strukturieren um den „Roten Faden“ nicht zu verlieren, auch Spannung darf hier nicht fehlen!

Der Schluss: 
Sollte knackig und kurz ausfallen; sollte die Kernpunkte aus dem Hauptteil nochmal aufgreifen.
Kann am Ende noch eine rhetorische Frage stellen über die das Publikum nachdenken kann  Führt auch zu Fragen zum Thema, was positiv sein kann, solange sie nicht das schon Erklärte betreffen 

## 5. Video Zusammenfassungen (Lessons Learned)

Gruppe1:<br>
So präsentiert ein Profi // Matthias Pöhm

Youtube: <https://www.youtube.com/watch?v=8O7CusRyWC4>

- einfache Metaphern verwenden
- Taktung kann eine sinnvolle Methode sein, um Botschaften zu betonen
- Powerpoint entwertet die eigenen Punkte
- Binde das Publikum in den Vortrag ein
- Eine gute Betonung kann wichtige Punkte verstärken
- Unterbewusstsein beeinflusst die Entscheidungen des Zuhörers stärker als der Verstand

Gruppe2:<br>
Amy Cuddy: Ihre Körpersprache beeinflusst, wer Sie sind (bis Minute 8)<br>
Youtube: <https://www.youtube.com/watch?v=Ks-_Mh1QhMc>

- Unsere Körpersprache beeinflusst unsere Gedanken, Gefühle & Auftreten.
- Trotz gleicher Qualifikation unterscheiden sich Leistungsergebnisse durch Körpersprache.

$$\rightarrow$$ Fake it ‘till you make it.

Gruppe3:<br>
Rhetorik? Braucht kein Mensch! // Michael Rossié<br>
Youtube: <https://www.youtube.com/watch?v=3KRk-iqO2jQ>

- Beim Aktiven Zuhören (“Hmm”, “Ja”, “Genau”, “Ahaa”) bekommt man nichts mit
- Position der Hände ist irrelevant; Hände lieber in die Hosentasche bevor man immer zur Hosentasche hin-zuckt.
- No-Gos:
  - “Ich will nicht eintönig klingen”-Melodie in den Satz bringen
  - Alle    2        Wörter         eine         Pause       machen     um       den       Satz      zu      strecken.
  - Letztes Wort im Satz betonen
  - ZU viele WÖRTER in EINEM Satz BETONEN.
  - Mit Ähs und Öhs den Satz strecken
  - “Nasig” oder mit einem “Lieblichen Hauch” sprechen

$$\rightarrow$$ Man braucht keine Rhetorik. Man muss einfach nur den Mut haben normal zu sein
$$\rightarrow$$ “Werden Sie wie Sie sind”

Gruppe4:<br>
Public Speaking: Warum der Rahmen wichtiger ist als der Inhalt // Tobias Beck<br>
Youtube: <https://www.youtube.com/watch?v=ywreVAx9rw8>

- Publikum zum Mitarbeiten/-Denken animieren
- jeder interessiert sich am meisten für sich selber $$\rightarrow$$ Bezug zum Publikum herstellen, z.b. warum ist thema
  wichtig für den Zuhörer
- grosse, bunte Bilder
- starke Sprache, keine konjunktive bzw. “Verpisserwoerter”
- mit viel Energie vortragen
- nicht normal sein, sondern Mut aufzufallen und Dinge unorthodox anzugehen
- Baut seine Argumente immer wieder auf einer Geschichte auf um das Publikum zum mitfiebern anzuregen
- viele unterstützende Gesten, Körpersprache
- interaktion mit dem Publikum

Gruppe5:<br>
Wie man in einem TEDx-talk clever wirkt | Will Stephen | TEDxNewYork (TOP)<br>
Youtube: <https://www.youtube.com/watch?v=8S0FDjFBj8o>

Einsatz von Mimik und Gestik um seine Aussagen zu untermalen, funktioniert sogar bei nichtssagenden Inhalten

Es gibt verschiedene Techniken, die mithilfe von Körpersprache verschiedene Situationen besser untermalen.

Selbst kleine Gesten, wie z.B. das Abnehmen der Brille können in gewissen Situationen bestimmte Gefühle ausdrücken.

Gruppe6:<br>
How to avoid death By PowerPoint | David JP Phillips | TEDxStockholmSalon<br>
Youtube: <https://www.youtube.com/watch?v=Iwpi1Lm6dFo>

- Folien nicht überladen (nicht mehr als 6 Objekte, lieber mehr Folien)
- Schwarzer Hintergrund, auch Bilder einarbeiten
- Fließtext nicht in Präsentation einarbeiten
- Wichtige Punkte sollten größer sein als die Überschrift
- Mit Kontrasten arbeiten (z.B. ausgrauen, hervorheben)

Gruppe7:
Julian Treasure: So reden, dass andere einem zuhören wollen
Youtube: <https://www.youtube.com/watch?v=eIho2S0ZahI>

- Mit Kontrast arbeiten
- Folien mit wenig Informationen
- Publikum mit einbeziehen (Aufwärmübung vor einen Vortrag)
- deutlich und langsam reden ⇒ gut verständlich
- nicht ruhig während des Vortrags an einer Stelle stehen
- Gestik und Mimik richtig einsetzen
- sei du selbst
- nicht monoton reden ⇒ Tonlage richtig einsetzen

Gruppe8:<br>
Präsentieren wie ein Profi | Tipps für Ihren überzeugenden Auftritt | #16<br>
Youtube: <https://www.youtube.com/watch?v=aFkKnSwMDsk&t=124s>

- Keine Langeweile
- Gute Sprechweise
- Sich zeigen
- Verständlichkeit
- Richtiger Fokus (auf das Publikum gerichtet)
- Präsent sein
- Gastgebermethode (sprechen wie zu Gästen)

Gruppe9:<br>
Drei Tipps für spannende Anfänge bei Vorträgen | Tipps für Ihren überzeugenden Auftritt | #1<br>
Youtube: <https://www.youtube.com/watch?v=yFACarKNKW4&t=87s>

1. Earcatcher Verwenden: Sorgt für Aufmerksamkeit
2. Interaktiver Anfang
   - Fragen ans Publikum - möglicherweise mit Objekten unterstützen
   - Frage muss zum Thema passen
   - Publikum muss innerlich mit JA antworten können
3. Bildhafter Einstieg
   - Bild muss gut vorstellbar sein und zum Thema hinführen
   - Nachteil: Sehr anspruchsvoll

Gruppe10:<br>
F-Secure - Cybercrime & Cyberwar - Rüdiger Trost - (Vortrag auf der ITSA 2015)<br>
Youtube: <https://2015.it-sa.tv/12047/f-secure-cybercrime-cyberwar>

- Viele schnelle Piktogramme, Grafiken, Schlagwörter 
- Persönliche Beispiele, Geschichte - Zuschauer fühlen sich eingebunden
- Einfache Darstellung von komplexen Inhalten
- Zurückführung auf Anfangs

Gruppe11:<br>
SySS - Live-Hacking: So brechen digitale Angreifer in Ihre Systeme ein - Sebastian Schreiber<br>
Youtube: <https://2017.it-sa.tv/8743/syss-live-hacking-so-brechen-digitale-angreifer-in-ihre-systeme-ein>

- Technik vorher vorbereiten und TESTEN!!1!
- Publikum einbinden
- freies Sprechen / selbstbewusstes Auftreten
- praktische Demonstration für die Anschaulichkeit
- Gliederung nicht zu groß
