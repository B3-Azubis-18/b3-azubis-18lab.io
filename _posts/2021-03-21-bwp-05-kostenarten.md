---
layout: post
title: "05. Kostenarten"
date: 2020-12-15 09:50:17 +0200
category: 3. Lehrjahr
tags: [ Betriebswirtschaftliche Prozesse]
---

Grundprinzip der Kalkulation (= Kostenträgerstückrechnung) ist die verursachungsgerechte Zuordnung der Kosten auf die
einzelnen Kostenträger. Aus der Summe der Kosten lassen sich zweierlei Kostenarten unterscheiden.

## Einzelkosten

Kosten, die einem Produkt direkt zurechenbar sind; nur dieses eine Produkt betreffen

- Materialkosten / Fertigungsmaterial (Rohstoffe)
- Fertigungslöhne
- Vertreterprovision für verkauftes Fertigerzeugnis (Sondereinzelkosten des Vertriebes)
- Spezialverpackung für Fertigerzeugnis (Sondereinzelkosten des Vertriebes)
- Spezialwerkzeug für die Produktion eines bestimmten Fertigerzeugnisses (Sondereinzelkosten der Fertigung)

## Gemeinkosten

Kosten, die einem Produkt nicht direkt zugerechnet werden können; die mehrere Produkte/das ganze Unternehmen betreffen

- Gehalt eines Meisters in der Dreherei
- Abschreibung auf die Fertigungsmaschine
- Abschreibung für Betriebsgebäude
- Heizungskosten
- Miete für eine Lagerhalle
- Stromkosten
- Kalkulatorische Zinsen für gelagerte Rohstoffe
- Kosten für Wasserverbrauch
