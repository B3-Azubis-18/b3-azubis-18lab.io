---
layout: post
title: "03. OOP Beziehungen"
date: 2020-12-20 08:30:00 +0200
category: 3. Lehrjahr
tags: [Anwendungsprogrammierung, Praxis]
---

## Grundsätzliches und Notation

Das Klassendiagramm ist die zentrale Diagrammart der UML1 . Sie stellt Klassen und deren Beziehungen untereinander dar.

Die folgende Abbildung zeigt die Notation einer Klasse:

![AWP - Beziehungen 01](/assets/img/AWP/03-beziehungen-01.png)

## Sichtbarkeiten

Eine etwas andere Darstellung einer Klasse zeigt, wie man Sichtbarkeiten von Eigenschaften und Methoden angeben kann:

![AWP - Beziehungen 02](/assets/img/AWP/03-beziehungen-02.png)

Das Minus-Zeichen steht für private: die Sichtbarkeit solcher Elemente ist auf die Klasse beschränkt, d.h. innerhalb
einer Methode derselben Klasse kann man auf private Elemente (Eigenschaften oder Methoden) uneingeschränkt zugreifen.
Von außen, also über ein Objekt der Klasse ist der Zugriff nicht möglich.

Soll über ein Objekt auf eine Eigenschaft oder Methode zugegriffen werden, so muss das entsprechende Element die
Sichtbarkeit `public` besitzen, im Klassendiagramm mit einem `+`-Zeichen gekennzeichnet. Eine dritte
Sichtbarkeitsbeschränkung ist `protected`; derart gekennzeichnete Elemente sind in der Klasse selbst (wie bei `private`)
und in von der Klasse abgeleiteten Klassen sichtbar. Mehr dazu im Kapitel Vererbung!

Grundsätzlich gilt bei der Vergabe von Sichtbarkeiten: So geschützt wie möglich und so offen wie nötig! Eine Spezialform
des Klassendiagramms ist das Objektdiagramm; hier sieht man den Zusammenhang zwischen Klasse und daraus instanziiertem
Objekt (dem Exemplar).

![AWP - Beziehungen 03](/assets/img/AWP/03-beziehungen-03.png)

Für die Objektdarstellung gilt folgende Notation:

![AWP - Beziehungen 04](/assets/img/AWP/03-beziehungen-04.png)

Methoden werden im Objektdiagramm nicht angegeben, weil sie bereits im Klassendiagramm beschrieben werden und für jedes
Objekt der Klasse identisch sind. Wichtige Angaben im Objektdiagramm sind der Name des Objektes (falls von Bedeutung)
und die Werte der Eigenschaften (Attributwerte).

![AWP - Beziehungen 05](/assets/img/AWP/03-beziehungen-05.png)

## UML und OOP Beziehungen

### Assoziation & Aggregation

![AWP - Beziehungen 06](/assets/img/AWP/03-beziehungen-06.png)

```cs
public class Motor{
    .....
    public void anlassen() {...}
}

public class Auto{
    .....
    public Motor myMotor = null;
}

static void Main(string[] args)
{
    Motor Austauschmotor = new Motor();
    Auto Toyota = new Auto();
    Toyota.myMotor = Austauschmotor;
    Toyota.myMotor.anlassen();
    Toyota = null;
    GC.Collect();
    // Auto ist zerstört, Austauschmotor noch nicht!
}
```

> Info: Umsetzung der Assoziation Hose-Mensch wie bei der Aggregation Motor-Auto!

### Komposition

![AWP - Beziehungen 07](/assets/img/AWP/03-beziehungen-07.png)

```cs
public class Nase
{
    public void niesen() {...}
}

public class Mensch
{
    private Nase myNase = null;

    public niesen()
    {
        myNase.niesen();
    }
    public Mensch()
    {
        this.myNase = new Nase();
    }
}

static void Main(string[] args)
{
    Mensch myMensch = new Mensch();
    myMensch.niesen();
    myMensch = null;
    GC.Collect();
    // Aufruf des GC -> Mensch und Nase zerstört!
}
```
