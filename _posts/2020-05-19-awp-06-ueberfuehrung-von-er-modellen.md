---
layout: post
title: "06. Von ER-Modellen zu logischen Modellen"
date: 2019-12-11 11:00:00 +0200
category: 2. Lehrjahr
tags: [Anwendungsprogrammierung]
---

## Vorgehensweise zur Überführung von ER-Modellen in logische Modelle

{% mermaid %}
graph TD;
    A[Eintitätsmengen in Relationen überführen]
    B[Primärschlüssel für jede Relation identifizieren]
    C[Beziehungen auflösen]
    D[1:n-Beziehung auflösen]
    E[1:1 Beziehung auflösen]
    F[n:m Beziehung auflösen]
    G{Kardinalität}
    A --> B
    B --> C
    C --> G
    G --> D
    G --> E
    G --> F
{% endmermaid %}

## 1:n Beziehung auflösen

{% mermaid %}
graph TD;
    A[1:n-Beziehung auflösen]
    B[Primärschlüssel der 1-Relation in n-Relation als Fremdschlüssel übertragen]
    C["1:Relation und n-Relation verbinden (Verbindung startet bei Primärschlüssel und endet bei Fremdschlüssel)"]
    D[1:n Kardinalität erfassen]
    
    A --> B
    B --> C
    C --> D
{% endmermaid %}

## 1:1 Beziehung auflösen

{% mermaid %}
graph TD;
    A[1:1-Beziehung auflösen]
    B{Variante?}
    C[Primärschlüssel der 1-Relation in andere 1-Relation als Fremdschlüssel übertragen]
    D[Einheitlichen Primärschlüssel für beide 1-Relationen identifizieren]
    E[Beide Relationen zu einer Relation zusammenfassen]
    F["Beide 1-Relationen verbinden (Verbindung startet bei Primärschlüssel und endet bei Fremdschlüssel)"]
    G["Beide 1-Relationen verbinden (Verbindung startet bei Primärschlüssel und endet bei anderen Primärschlüssel)"]
    H[1:1-Kardinalität erfassen]
    
    A --> B
    B --> C
    B --> D
    B --> E
    C --> F
    D --> G
    F --> H
    G --> H
{% endmermaid %}

## n:m Beziehung auflösen

{% mermaid %}
graph TD;
    A[n:m-Beziehung auflösen]
    B[Neue Relation anlegen]
    C[Primärschlüssel der ursprünglichen Relation als Fremdschlüssel in die neue Relation übernehmen]
    D[Beide Fremdschlüssel der neuen Relation als Primärschlüssel kennzeichnen]
    E["Urpsrüngliche Relationen mit der neuen Relation verbinden (Verbindung stratet bei Primärschlüssel und ende bei entsprechendem Fremdschlüssel)"]
    F[1:n-Kardinalitäten erfassen]
    
    A --> B
    B --> C
    C --> D
    D --> E
    E --> F
{% endmermaid %}
