#! /bin/sh
# Usage:
# ./md2post.sh filename title tags 
header="""---\nlayout: post\ntitle: \"$2\"\ndate: $(date +"%F %H:%M:%S %z")\ncategory: 3. Lehrjahr\ntags: [$3]\n---\n"""

filename=$(realpath "$1")
temp_file=$(mktemp)
echo -e $header | cat - "$filename" > $temp_file && mv "$temp_file" "$filename"
