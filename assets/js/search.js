(function () {
    // https://gist.github.com/codeguy/6684588
    function string_to_slug (str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to   = "aaaaeeeeiiiioooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return str;
    }

    function displaySearchResults(results, store) {
        var searchResults = document.getElementById('search-results');

        if (results.length) { // Are there any results?
            let appendString = '';

            for (let i = 0; i < results.length; i++) {  // Iterate over the results
                const ref = results[i]['ref'];
                const item = store.find(element => element["name"] === ref);
                appendString += '<li>';
                appendString += '<a href="' + item.url + '">' + item.title + '</a> - ';
                appendString += '<a href="/category.html/#' + string_to_slug(item.category) + '">' + item.category + '</a>' + ' - ';
                item.tags.forEach(tag => {
                    appendString += '<a className="codinfox-tag-mark" href="/tags/#' + string_to_slug(tag) + '">' + tag + '</a>&nbsp;';
                    }
                )
                appendString += '<p>' + item.content.substring(0, 150) + '...</p>';
                appendString += '</li>';
            }

            searchResults.innerHTML = appendString;
        } else {
            searchResults.innerHTML = '<li>No results found</li>';
        }
    }

    function getQueryVariable(variable) {
        const query = window.location.search.substring(1);
        const vars = query.split('&');

        for (let i = 0; i < vars.length; i++) {
            const pair = vars[i].split('=');

            if (pair[0] === variable) {
                return decodeURIComponent(pair[1].replace(/\+/g, '%20'));
            }
        }
    }

    const searchTerm = getQueryVariable('query');

    if (searchTerm) {
        document.getElementById('search-box').setAttribute("value", searchTerm);

        // Initalize lunr with the fields it will be searching on. I've given title
        // a boost of 10 to indicate matches on this field are more important.
        var idx = lunr(function () {
            this.ref('name')
            this.field('title')
            this.field('category')
            this.field('tags')
            this.field('content')
            this.field('url')

            window.store.forEach(function (doc) {
                this.add(doc)
            }, this)
        })

        var results = idx.search(searchTerm); // Get lunr to perform a search
        displaySearchResults(results, window.store); // We'll write this in the next section
    }
})();
