# B3-Azubis Website with class notes

## Creating a new post

Filename rules: `YYYY-MM-DD-subject-00-title-of-post.md`
- `00` is an ascending number which starts from 1.
- `YYYY-MM-DD` is the date on which you are submitting the post. It is important that the order of topics related to the
  submission date is correct.
- If you are submitting a post for a missing topic use a date and time which is historically between the topic after
  this one and before the previous. This overrides the rule that you should you use the date of your submitting day.

Please note that the posts should not contain any kind of exercises at the current state of the website. Examples
although are appreciated for explaining purposes.  
